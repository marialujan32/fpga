----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2022 10:14:47
-- Design Name: 
-- Module Name: FSM_acond_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM_acond_tb is
--  Port ( );
end FSM_acond_tb;

architecture Behavioral of FSM_acond_tb is
component signal_acond
        Port (
            clk : in std_logic;
            botones : in std_logic_vector (0 to 3);
            bloqueo : in std_logic;
            acond_botones : out std_logic_vector (0 to 3);
            acond_bloqueo : out std_logic
        );
    end component;
    
component FSM_psw
    generic(contra:std_logic_vector);
        Port (
            clk : in std_logic;
            EN : in std_logic;
            botones : in std_logic_vector (0 to 3);
            bloqueo : in std_logic;
            reset : in std_logic;
            code : out std_logic_vector (0 to 4);
            salidaOK : out std_logic;
            desbloquea : out std_logic
        );
end component;

        signal clk :  std_logic := '0';
        signal EN : std_logic;
        signal botones :  std_logic_vector (0 to 3);
        signal bloqueo:  std_logic;
        signal acond_botones :  std_logic_vector (0 to 3);
        signal acond_bloqueo:  std_logic;        
        signal reset : std_logic;
        signal code : std_logic_vector (0 to 4);
        signal salidaOK : std_logic;
        signal desbloquea : std_logic;
        constant period : time := 10 ns;
begin
dut1: signal_acond port map (
            clk           => clk,
            botones       => botones,
            bloqueo       => bloqueo,
            acond_botones => acond_botones,
            acond_bloqueo => acond_bloqueo
);
dut2: FSM_psw 
generic map (contra => "0001001000011000")
port map (
            clk        => clk,
            EN         => EN,
            botones    => acond_botones,
            bloqueo    => acond_bloqueo,
            reset      => reset,
            code       => code,
            salidaOK   => salidaOK,
            desbloquea => desbloquea
);
bloqueo <= '0', '1' after 54 ms, '0' after 65 ms; 
EN <= '0', '1' after 3ms;
reset <= '1', '0' after 78ms, '1' after 79ms;       
botones <= "0000" ,
"0001" after 5  ms,"0000" after 16 ms,
"0010" after 17 ms,"0000" after 29 ms,
"0001" after 30 ms,"0000" after 41 ms,
"1000" after 42 ms,"0000" after 53 ms,
"1000" after 66 ms,"0000" after 77 ms,
"1000" after 79 ms,"0000" after 80 ms;

clk<= not clk after 0.5*period;              

assert false        -- Forzar fallo
report "[SUCCESS]: simulation finished OK."
severity failure;   -- M�xima severidad para garantizar parada
end Behavioral;
