library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity acond_ent_tb is
--  Port ( );
end acond_ent_tb;

architecture sim of acond_ent_tb is
component signal_acond
        Port (
            clk : in std_logic;
            botones : in std_logic_vector (0 to 3);
            bloqueo : in std_logic;
            acond_botones : out std_logic_vector (0 to 3);
            acond_bloqueo : out std_logic
        );
    end component;
    signal clk :  std_logic := '0';
    signal botones :  std_logic_vector (0 to 3);
    signal bloqueo:  std_logic;
    signal acond_botones : std_logic_vector (0 to 3);
    signal acond_bloqueo :  std_logic;
    constant period : time := 10 ns;

begin
acondicionamiento_entradas: signal_acond port map(
            clk           => clk,
            botones       => botones,
            bloqueo       => bloqueo,
            acond_botones => acond_botones,
            acond_bloqueo => acond_bloqueo
    
        );
bloqueo <= '0', '1' after 53 ms, '0' after 64 ms;        
botones <= "0000" ,
"1000" after 5  ms ,"0000" after 16 ms,
"0001" after 17 ms,"0000" after 28 ms,
"0010" after 29 ms,"0000" after 40 ms,
"0100" after 41 ms,"0000" after 52 ms,
"0100" after 63 ms,"0000" after 74 ms,
"1000" after 75 ms,"0000" after 79 ms;

  
clk<= not clk after 0.5*period;              

assert false        -- Forzar fallo
report "[SUCCESS]: simulation finished OK."
severity failure;   -- M�xima severidad para garantizar parada
end sim;
