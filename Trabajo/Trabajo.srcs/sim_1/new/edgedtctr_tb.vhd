----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2022 00:58:43
-- Design Name: 
-- Module Name: edgedtctr_tb - ed_tb
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity edgedtctr_tb is
--  Port ( );
end edgedtctr_tb;

architecture ed_tb of edgedtctr_tb is
component edge_detector is
        port(
            CLK : in std_logic;
            SYNC_IN : in std_logic;
            EDGE : out std_logic
        );
    end component;
    signal clk :  std_logic := '0';
    signal SYNC_IN  :  std_logic := '0';
    signal EDGE :  std_logic;
    constant period : time := 10 ns;
begin
dut: edge_detector PORT MAP(
              clk     => clk,
              SYNC_IN  => SYNC_IN,
              EDGE => EDGE
);

SYNC_IN <= '0',
          '1' after 10ns, '0' after 5ms , '1' after 6ms, '0' after 16ms+10ns,
          '1' after 17ms, '0' after 25ms, '1' after 25ms + 10ns,
          '0' after 40ms, '1' after 41ms, '0' after 61ms, '1' after 62ms, '0' after 65ms,
          '1' after 70ms, '0' after 76ms;

clk<= not clk after 0.5*period;              

assert false        -- Forzar fallo
report "[SUCCESS]: simulation finished OK."
severity failure;   -- M�xima severidad para garantizar parada

end ed_tb;
