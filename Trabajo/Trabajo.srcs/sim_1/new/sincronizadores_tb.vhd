library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sincronizadores_tb is
    --  Port ( );
end sincronizadores_tb;

architecture testbench of sincronizadores_tb is
    component sync is
        port(
            CLK : in std_logic;
            ASYNC_IN : in std_logic;
            SYNC_OUT : out std_logic
        );
    end component;
    signal clk :  std_logic := '0';
    signal ASYNC_IN  :  std_logic := '0';
    signal SYNC_OUT :  std_logic;
    constant period : time := 10 ns;

begin
    dut: sync port map(
            clk => clk,
            ASYNC_IN => ASYNC_IN,
            SYNC_OUT => SYNC_OUT
        );

ASYNC_IN <= not ASYNC_IN after 0.7*period;

clk<= not clk after 0.5*period;              

assert false        -- Forzar fallo
report "[SUCCESS]: simulation finished OK."
severity failure;   -- M�xima severidad para garantizar parada

end testbench;
