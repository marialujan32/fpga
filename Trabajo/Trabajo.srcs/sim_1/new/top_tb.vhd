----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.11.2021 20:46:08
-- Design Name: 
-- Module Name: top_tb - test
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_tb is
--  Port ( );
end top_tb;

architecture test of top_tb is
component top is
Port (
        clk : in std_logic;
        bloqueo : in std_logic;
        reset : in std_logic;
        EN : in std_logic;
        botones : in std_logic_vector (0 to 3);
        salidaOK : out std_logic;
        desbloquea : out std_logic;
        segmentDECODED : out std_logic_vector(0 to 7);
        segmentMSG : out std_logic_vector(0 to 6)
    );
end component;
signal clk :  std_logic := '0';
signal bloqueo : std_logic;
signal reset : std_logic;
signal EN : std_logic;
signal botones :  std_logic_vector (0 to 3);
signal salidaOK :  std_logic;
signal desbloquea : std_logic;
signal segmentDECODED :  std_logic_vector(0 to 7);
signal segmentMSG :  std_logic_vector(0 to 6);
constant period : time := 10 ns;

begin

dut: top 
PORT MAP(
              clk        => clk,
              bloqueo    => bloqueo,
              reset      => reset,
              EN         => EN,
              botones    => botones,
              salidaOK   => salidaOK,
              desbloquea => desbloquea,
              segmentDECODED => segmentDECODED,
              segmentMSG => segmentMSG
);
EN <= '0', '1' after 3 ms;
reset<= '1', '0' after 77 ms, '1' after 78 ms;
botones <= "0000" ,
"0100" after 5  ms ,"0000" after 16 ms,
"1000" after 17 ms,"0000" after 28 ms,
"0001" after 29 ms,"0000" after 40 ms,
"0001" after 41 ms,"0000" after 52 ms,
"0100" after 65 ms,"0000" after 76 ms,
"1000" after 79 ms,"0000" after 80 ms;
  
clk<= not clk after 0.5*period;              

bloqueo<='0', '1' after 53 ms, '0' after 64ms  ;
          
          
          
          
          
assert salidaOK = '1'
report "Salida activada"
severity note;        
          
assert false        -- Forzar fallo
report "[SUCCESS]: simulation finished OK."
severity failure;   -- M�xima severidad para garantizar parada

end test;
