library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity segments_tb is
--  Port ( );
end segments_tb;

architecture Behavioral of segments_tb is

component segments
    Port (
            clk : in std_logic;
            code : in std_logic_vector(0 to 4);
            segmentAN : out std_logic_vector(2 downto 0);
            segmentMSG : out std_logic_vector(0 to 6)
     );
     end component;

 signal clk :  std_logic := '0';
 signal code :  std_logic_vector(0 to 4);
 signal segmentAN :  std_logic_vector(2 downto 0);
 signal segmentMSG :  std_logic_vector(0 to 6);
 constant period : time := 10 ns;     
begin

dut: segments PORT MAP(
              clk         => clk,
              code        => code,
              segmentAN   => segmentAN,
              segmentMSG  => segmentMSG
);
          clk<= not clk after 0.5*period;              
process
begin
          code<= (others => '0');
          wait for 30 ns;
          code<= "10000";
          wait for 10ms;
          code<= "01000";
          wait for 10ms;
          code<= "00100";
          wait for 10ms;
          code<= "00010";
          wait for 10ms;
          code<= "00001";
          wait for 10ms;
          code<= "11111";
          wait for 10ms;
          code<= "00000";
          wait for 10ms;
          code<= "00001";
          wait for 5ms;
          code<= "11111";
          wait for 3ms;
          code<= "10000";
          
    
  end process;
  assert false
  	report "[SUCCESS]: Simulation finished."
  	severity failure ;
end Behavioral;
