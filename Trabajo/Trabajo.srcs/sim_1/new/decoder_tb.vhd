library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_tb is
    --  Port ( );
end decoder_tb;

architecture Behavioral of decoder_tb is
    component decoder
        Port (
            code : in std_logic_vector(2 downto 0);
            segmentDECODED : out std_logic_vector(0 to 7)
        );
    end component;
    signal code :  std_logic_vector(2 downto 0);
    signal segmentDECODED :  std_logic_vector(0 to 7);

begin
    dut: decoder port map(
            code        => code,
            segmentDECODED  => segmentDECODED
        );
    
process
begin
          code<= "000";
          wait for 10ms;
          code<= "001";
          wait for 10ms;
          code<= "010";
          wait for 10ms;
          code<= "011";
          wait for 10ms;
          code<= "100";
          wait for 10ms;
          code<= "101";
          wait for 10ms;
          code<= "110";
          wait for 10ms;
          code<= "111";
          wait for 10ms;

          
    
  end process;
  assert false
  	report "[SUCCESS]: Simulation finished."
  	severity failure;
end Behavioral;
