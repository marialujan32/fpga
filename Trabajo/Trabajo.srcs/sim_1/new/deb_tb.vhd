library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity deb_tb is
--  Port ( );
end deb_tb;

architecture Behavioral of deb_tb is
component debouncer is 
Port ( 
        clk : in std_logic;
        deb_in : in std_logic;
        deb_out : out std_logic 
      );
end component;
signal clk :  std_logic := '0';
signal deb_in :  std_logic;
signal deb_out:  std_logic;
constant period : time := 10 ns;      

begin

dut: debouncer PORT MAP(
              clk     => clk,
              deb_in  => deb_in,
              deb_out => deb_out
);

          clk<= not clk after 0.5*period;              
          deb_in <= '0',
          '1' after 10ns, '0' after 5ms , '1' after 6ms, '0' after 16ms+10ns,
          '1' after 17ms, '0' after 25ms, '1' after 25ms + 10ns,
          '0' after 40ms, '1' after 41ms, '0' after 61ms, '1' after 62ms, '0' after 65ms,
          '1' after 70ms, '0' after 76ms; 

                
    assert false
  	report "[SUCCESS]: Simulation finished."
  	severity failure ;
end Behavioral;
