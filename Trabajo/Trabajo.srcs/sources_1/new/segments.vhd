library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity segments is
    generic(counter_size : integer := 19); -- 19 bits => 5.24 ms maximo con clk 100MHz
    Port (
        clk : in std_logic;
        code : in std_logic_vector(0 to 4);
        segmentAN : out std_logic_vector(2 downto 0);
        segmentMSG : out std_logic_vector(0 to 6)
    );
end segments;

architecture Behavioral of segments is
    signal AN : unsigned(2 downto 0) := "000";
    signal count_time : unsigned(0 to counter_size-1) := to_unsigned(0,19);
    type mensajes is array (integer range 0 to 7) of std_logic_vector(6 downto 0);                                                -- Codificación para cada caracter
    constant off : mensajes      := ("1111111", "1111111", "1111111", "1111111", "1111111", "0000001", "0111000", "0111000");     -- O F F
    constant open_msg : mensajes := ("1111111", "1111111", "1111111", "1111111", "0000001", "0011000", "0110000", "1101010");     -- O P E N
    constant closed : mensajes   := ("1111111", "1111111", "0110001", "1110001", "0000001", "0100100", "0110000", "1000010");     -- C L O S E D
    signal salida : mensajes;

begin
---------------------------------------------
-- Salidas
    segmentAN <= std_logic_vector(AN);
    segmentMSG <= salida(to_integer(AN));
---------------------------------------------

    refresco_7segmentos:process(clk)
    begin
        if rising_edge(clk) then
            if count_time = to_unsigned(1E5,19) then    -- Tiempo tras el que se refrescara cada display del 7 segmentos, 1ms;
                if AN = 7 then
                    AN <= "000";
                else
                    AN <= AN + 1;
                end if;
                count_time <= (others => '0');
            else
                count_time <= count_time + 1;
            end if;
        end if;
    end process;


    asignacion_mensajes:process(code)   -- Codificacion para los digitos segun el codigo recibido
    begin
        case code is
            when "11111" =>
                salida <= off;

            when "00001" =>
                salida <= open_msg;

            when others =>
                salida <= closed;

        end case;
    end process;

end Behavioral;

------------------------------------------- 
--"0110001"  -- C
--"1110001"  -- L
--"0000001"  -- O
--"0100100"  -- S
--"0110000"  -- E
--"1000010"  -- d
------------------------------------------- 
--"0000001"  -- O
--"0111000"  -- F
--"0111000"  -- F
------------------------------------------- 
--"0000001"  -- O
--"0011000"  -- P 
--"0110000"  -- E
--"1101010"  -- n
------------------------------------------- 