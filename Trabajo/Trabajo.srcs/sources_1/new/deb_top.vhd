----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.11.2021 13:46:13
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity deb_top is
    Port (
        CLK : in std_logic;
        deb_in_botones : in std_logic_vector (0 to 3);
        deb_in_bloqueo : in std_logic;
        deb_out_botones : out std_logic_vector (0 to 3);
        deb_out_bloqueo : out std_logic
    );
end deb_top;

architecture debouncers of deb_top is
    component debouncer
        Port (
            clk : in std_logic;
            deb_in : in std_logic;
            deb_out : out std_logic
        );
    end component;
begin

    deb_boton1: debouncer port map(
            clk=>clk,
            deb_in=>deb_in_botones(0),
            deb_out=>deb_out_botones(0)
        );
    deb_boton2: debouncer port map(
            clk=>clk,
            deb_in=>deb_in_botones(1),
            deb_out=>deb_out_botones(1)
        );
    deb_boton3: debouncer port map(
            clk=>clk,
            deb_in=>deb_in_botones(2),
            deb_out=>deb_out_botones(2)
        );
    deb_boton4: debouncer port map(
            clk=>clk,
            deb_in=>deb_in_botones(3),
            deb_out=>deb_out_botones(3)
        );
    deb_botonOK: debouncer port map(
            clk=>clk,
            deb_in=>deb_in_bloqueo,
            deb_out=>deb_out_bloqueo
        );

end debouncers;
