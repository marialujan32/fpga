library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_arith.ALL;
use ieee.std_logic_unsigned.ALL;



entity decoder is
    port (
        code : in std_logic_vector(2 downto 0);
        segmentDECODED : out std_logic_vector(0 to 7)
    );
end entity decoder;


architecture dataflow of decoder is
begin
    with code select
     segmentDECODED <=  "11111110" when "000",  -- Codificacion para atacar sobre los distintos 7 segmentos seg�n la entrada code, AN en segments
                        "11111101" when "001",
                        "11111011" when "010",
                        "11110111" when "011",
                        "11101111" when "100",
                        "11011111" when "101",
                        "10111111" when "110",
                        "01111111" when "111",
                        "11111111" when others;
end architecture dataflow;
