----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.12.2021 14:40:09
-- Design Name: 
-- Module Name: debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
    generic(counter_size : integer := 20); --20 bits => 10.48 ms maximo con clk 100MHz
    Port (
        clk : in std_logic;
        deb_in : in std_logic;
        deb_out : out std_logic
    );
end debouncer;

architecture logic of Debouncer is
    signal count_time : unsigned(0 to counter_size-1);
    signal reg : std_logic;
begin

    process(clk,deb_in)
    begin
        deb_out <= reg ;
        if deb_in = '0' then
            count_time <= to_unsigned(0,20);
            reg <= '0';
        elsif (clk'event and clk = '1') then
            if deb_in = '1' then
                if count_time = to_unsigned(1E6,20) then   --Espera 10 ms en los que la entrada debe estar en alto
                    reg<= '1';
                else
                    count_time <= count_time + 1;          --Suma ticks de reloj hasta llegar al valor indicado arriba
                end if;
            end if;
        end if;
    end process;
end logic;
