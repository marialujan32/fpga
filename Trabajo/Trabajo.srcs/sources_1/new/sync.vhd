library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sync is
    port (
        CLK : in std_logic;
        ASYNC_IN : in std_logic;
        SYNC_OUT : out std_logic
    );
end sync;
architecture BEHAVIORAL of sync is
    signal sreg : std_logic_vector(1 downto 0):="00";
begin
    process (CLK)
    begin
        if rising_edge(CLK) then
            sync_out <= sreg(1);
            sreg <= sreg(0) & async_in;     -- Desplaza el segundo bit y escribe la entrada en el primero a golpe de reloj
        end if;
    end process;
end BEHAVIORAL;