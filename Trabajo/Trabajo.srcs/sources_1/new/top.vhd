----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.11.2021 19:49:36
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port (
        clk : in std_logic;
        bloqueo : in std_logic;
        reset : in std_logic;
        EN : in std_logic;
        botones : in std_logic_vector (0 to 3);
        salidaOK : out std_logic;
        desbloquea : out std_logic;
        segmentDECODED : out std_logic_vector(0 to 7);
        segmentMSG : out std_logic_vector(0 to 6)

    );
    signal acond_out_botones: std_logic_vector (0 to 3);
    signal acond_out_bloqueo: std_logic;
    signal code: std_logic_vector (0 to 4);
    signal segmentAN: std_logic_vector(2 downto 0);
end top;

architecture inst of top is
-- Declaracion de los distintos componentes
    component signal_acond
        Port (
            clk : in std_logic;
            botones : in std_logic_vector (0 to 3);
            bloqueo : in std_logic;
            acond_botones : out std_logic_vector (0 to 3);
            acond_bloqueo : out std_logic
        );
    end component;
    
    component FSM_psw
    generic(contra:std_logic_vector);
        Port (
            clk : in std_logic;
            EN : in std_logic;
            botones : in std_logic_vector (0 to 3);
            bloqueo : in std_logic;
            reset : in std_logic;
            code : out std_logic_vector (0 to 4);
            salidaOK : out std_logic;
            desbloquea : out std_logic
        );
    end component;

    component segments
        Port (
            clk : in std_logic;
            code : in std_logic_vector(0 to 4);
            segmentAN : out std_logic_vector(2 downto 0);
            segmentMSG : out std_logic_vector(0 to 6)
        );
    end component;

    component decoder
        Port (
            code : IN std_logic_vector(2 DOWNTO 0);
            segmentDECODED : OUT std_logic_vector(0 to 7)
        );
    end component;

begin
-- Instanciacion de los componentes
    acondicionamiento_entradas: signal_acond port map(
            clk           => clk,
            botones       => botones,
            bloqueo       => bloqueo,
            acond_botones => acond_out_botones,
            acond_bloqueo => acond_out_bloqueo
    
        );
    maquina_de_estado: FSM_psw
     generic map (contra => "0100100000010001")--1000000100100100
     port map(
            clk        => clk,
            EN         => EN,
            botones    => acond_out_botones,
            bloqueo    => acond_out_bloqueo,
            reset      => reset,
            code       => code,
            salidaOK   => salidaOK,
            desbloquea => desbloquea
        );
    segmentos: segments port map(
            clk         => clk,
            code        => code,
            segmentAN   => segmentAN,
            segmentMSG  => segmentMSG
        );
    decoder_7segmentos: decoder port map(
            code            => segmentAN,
            segmentDECODED  => segmentDECODED
        );
end inst;