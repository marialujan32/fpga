library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity edge_detector is
    port (
        CLK : in std_logic;
        SYNC_IN : in std_logic;
        EDGE : out std_logic
    );
end edge_detector;

architecture BEHAVIORAL of edge_detector is
    signal sreg : std_logic_vector(2 downto 0);
begin
    process (CLK)
    begin
        if rising_edge(CLK) then
            sreg <= sreg(1 downto 0) & SYNC_IN;     --Desplaza los bits 1 y 0 hacia la izquierda y asigna la entrada al bit restante
        end if;
    end process;
    
    with sreg select
         EDGE <= '1' when "100",                    -- El unico caso en el que la salida valdra '1' sera cuando tras haber pulsado un boton
                 '0' when others;                   -- le sigan dos '0', es decir cuando se deje de pulsar dicho boton
end BEHAVIORAL;