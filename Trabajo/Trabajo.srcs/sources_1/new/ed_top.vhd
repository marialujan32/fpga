----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.11.2021 14:00:49
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ed_top is
    port (
        CLK : in std_logic;
        ed_in_botones : in std_logic_vector (0 to 3);
        ed_in_bloqueo : in std_logic;
        ed_botones : out std_logic_vector (0 to 3);
        ed_bloqueo : out std_logic
    );
end ed_top;

architecture detectoresflanco of ed_top is
    component edge_detector
        port (
            CLK : in std_logic;
            SYNC_IN : in std_logic;
            EDGE : out std_logic
        );
    end component;
begin
    ed_boton1: edge_detector port map(
            clk=>clk,
            sync_in=>ed_in_botones(0),
            edge=>ed_botones(0)
        );
    ed_boton2: edge_detector port map(
            clk=>clk,
            sync_in=>ed_in_botones(1),
            edge=>ed_botones(1)
        );
    ed_boton3: edge_detector port map(
            clk=>clk,
            sync_in=>ed_in_botones(2),
            edge=>ed_botones(2)
        );
    ed_boton4: edge_detector port map(
            clk=>clk,
            sync_in=>ed_in_botones(3),
            edge=>ed_botones(3)
        );
    edge_botonOK: edge_detector port map(
            clk=>clk,
            sync_in=>ed_in_bloqueo,
            edge=>ed_bloqueo
        );

end detectoresflanco;
