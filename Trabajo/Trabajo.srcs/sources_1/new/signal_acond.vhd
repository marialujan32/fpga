library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity signal_acond is
    Port (
        clk : in std_logic;
        botones : in std_logic_vector (0 to 3);
        bloqueo : in std_logic;
        acond_botones : out std_logic_vector (0 to 3);
        acond_bloqueo : out std_logic
    );
    signal sync_out_botones: std_logic_vector (0 to 3);
    signal sync_out_bloqueo: std_logic;
    signal deb_out_botones: std_logic_vector (0 to 3);
    signal deb_out_bloqueo: std_logic;
end signal_acond;

architecture struct of signal_acond is

    component sync_top
        port(
            CLK : in std_logic;
            sync_in_botones : in std_logic_vector (0 to 3);
            sync_in_bloqueo : in std_logic;
            sync_out_botones : out std_logic_vector (0 to 3);
            sync_out_bloqueo : out std_logic
        );
    end component;

    component deb_top
        port(
            CLK : in std_logic;
            deb_in_botones : in std_logic_vector (0 to 3);
            deb_in_bloqueo : in std_logic;
            deb_out_botones : out std_logic_vector (0 to 3);
            deb_out_bloqueo : out std_logic
        );
    end component;

    component ed_top
        port (
            CLK : in std_logic;
            ed_in_botones : in std_logic_vector (0 to 3);
            ed_in_bloqueo : in std_logic;
            ed_botones : out std_logic_vector (0 to 3);
            ed_bloqueo : out std_logic
        );
    end component;
begin
    sincronizadores: sync_top port map(
            clk=>clk,
            sync_in_botones => botones,
            sync_in_bloqueo => bloqueo,
            sync_out_botones=> sync_out_botones,
            sync_out_bloqueo=> sync_out_bloqueo
        );
    antirrebotes: deb_top port map(
            clk=>clk,
            deb_in_botones => sync_out_botones,
            deb_in_bloqueo => sync_out_bloqueo,
            deb_out_botones=> deb_out_botones,
            deb_out_bloqueo=> deb_out_bloqueo
        );
    detectores_de_flanco: ed_top port map(
            clk=>clk,
            ed_in_botones=> deb_out_botones,
            ed_in_bloqueo=> deb_out_bloqueo,
            ed_botones   => acond_botones,
            ed_bloqueo   => acond_bloqueo
        );

end struct;
