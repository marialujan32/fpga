library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FSM_psw is
    generic(contra:std_logic_vector);
    Port (
        clk : in std_logic;
        EN : in std_logic;
        botones : in std_logic_vector (0 to 3);
        bloqueo : in std_logic;
        reset : in std_logic;
        code : out std_logic_vector (0 to 4);
        salidaOK : out std_logic;
        desbloquea : out std_logic
    );
    signal psw: std_logic_vector (0 to 15):= contra;
end FSM_psw;

architecture arc of FSM_psw is
    type estados is (S0, S1, S2, S3, S4, S100);
    signal estado: estados := S0;
    signal sig_estado: estados;
    signal psw1,psw2,psw3,psw4 : std_logic_vector (0 to 3);

begin
-------------------------------------------------------------------------------------------
-- Asignacion del generico a se�ales por cada digito de la contrase�a
    psw1<=psw(0 to 3);
    psw2<=psw(4 to 7);
    psw3<=psw(8 to 11);
    psw4<=psw(12 to 15);
-------------------------------------------------------------------------------------------
    state_register:process (clk,reset) -- Proceso encargado de la gestion de estados con reset y enabler
    begin
        if clk'event and clk ='1' then
            if EN = '1' then
                if reset ='0' then
                    estado <= S0;
                else
                    estado <= sig_estado;
                end if;
            else
                estado <= S100;
            end if;
        end if;
    end process;

    next_state_decod:process (botones, bloqueo, estado) -- Proceso encargado de la codificacion de estados
    begin
        sig_estado <= estado;
        case estado is

            when S0 =>
                if botones = psw1 then sig_estado <= S1;        -- Primer boton correcto para pasar a S1
                elsif botones /= "0000" then sig_estado <= S0;  -- Boton incorrecto devuelve a S0
                end if;
            when S1 =>
                if botones = psw2 then sig_estado <= S2;        -- Segundo boton correcto para pasar a S2
                elsif botones = psw1 then sig_estado <= S1;     -- Si se pulsa primer boton de la secuencia pasa a S1
                elsif botones /= "0000" then sig_estado <= S0;  -- Boton incorrecto devuelve a S0
                end if;
            when S2 =>
                if botones = psw3 then sig_estado <= S3;        -- Tercer boton correcto para pasar a S3
                elsif botones = psw1 then sig_estado <= S1;     -- Si se pulsa primer boton de la secuencia pasa a S1
                elsif botones /= "0000" then sig_estado <= S0;  -- Boton incorrecto devuelve a S0
                end if;
            when S3 =>
                if botones = psw4 then sig_estado <= S4;        -- Cuarto boton correcto para pasar a S4
                elsif botones = psw1 then sig_estado <= S1;     -- Si se pulsa primer boton de la secuencia pasa a S1
                elsif botones /= "0000" then sig_estado <= S0;  -- Boton incorrecto devuelve a S0
                end if;
            when S4 =>
                if bloqueo = '1' then sig_estado <= S0;         -- Se espera una pulsacion del boton central, bloqueo
                end if;
            when S100 =>                                        -- Estado con EN = '0'
                sig_estado <= S0;

            when others =>
                sig_estado <= S0;

        end case;
    end process;

    output_decod:process (estado) -- Proceso encargado de la gestion de salidas de la FSM          
    begin
        case estado is
            when S0 =>
                code <= "10000";                                -- Codigos para el uso del 7 segmentos
                salidaOK <= '0';
                desbloquea <= '0';
            when S1 =>
                code <= "01000";
                salidaOK <= '0';
                desbloquea <= '0';
            when S2 =>
                code <= "00100";
                salidaOK <= '0';
                desbloquea <= '0';
            when S3 =>
                code <= "00010";
                salidaOK <= '0';
                desbloquea <= '0';
            when S4 =>
                code <= "00001";
                salidaOK <= '1';                                -- LED que indica que se ha introducido la contrase�a correcta    
                desbloquea <= '1';                              -- Salida PMOD alto
            when S100 =>
                code <= "11111";
                salidaOK <= '0';
                desbloquea <= '0';
            when others =>
                code <= "00000";
                salidaOK <= '0';
                desbloquea <= '0';

        end case;
    end process;

end arc;