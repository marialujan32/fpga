----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.11.2021 13:46:13
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sync_top is
    port(
        CLK : in std_logic;
        sync_in_botones : in std_logic_vector (0 to 3);
        sync_in_bloqueo : in std_logic;
        sync_out_botones : out std_logic_vector (0 to 3);
        sync_out_bloqueo : out std_logic
    );
end sync_top;

architecture sincronizadores of sync_top is
    component sync
        port(
            CLK : in std_logic;
            ASYNC_IN : in std_logic;
            SYNC_OUT : out std_logic
        );
    end component;
begin

    sync_boton1: sync port map(
            clk=>clk,
            async_in=>sync_in_botones(0),
            sync_out=>sync_out_botones(0)
        );
    sync_boton2: sync port map(
            clk=>clk,
            async_in=>sync_in_botones(1),
            sync_out=>sync_out_botones(1)
        );
    sync_boton3: sync port map(
            clk=>clk,
            async_in=>sync_in_botones(2),
            sync_out=>sync_out_botones(2)
        );
    sync_boton4: sync port map(
            clk=>clk,
            async_in=>sync_in_botones(3),
            sync_out=>sync_out_botones(3)
        );
    sync_botonOK: sync port map(
            clk=>clk,
            async_in=>sync_in_bloqueo,
            sync_out=>sync_out_bloqueo
        );

end sincronizadores;
