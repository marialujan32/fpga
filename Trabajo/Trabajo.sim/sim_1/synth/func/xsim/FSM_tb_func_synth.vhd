-- Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2021.1 (win64) Build 3247384 Thu Jun 10 19:36:33 MDT 2021
-- Date        : Tue Jan 18 02:41:08 2022
-- Host        : DESKTOP-0UU3ISI running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/alvar/Documents/Sourcetree/fpga/Trabajo/Trabajo.sim/sim_1/synth/func/xsim/FSM_tb_func_synth.vhd
-- Design      : top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity FSM_psw is
  port (
    estado : out STD_LOGIC_VECTOR ( 2 downto 0 );
    segmentMSG_OBUF : out STD_LOGIC_VECTOR ( 1 downto 0 );
    salidaOK_OBUF : out STD_LOGIC;
    \estado_reg[0]_0\ : out STD_LOGIC;
    \estado_reg[2]_0\ : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    \estado_reg[0]_1\ : in STD_LOGIC;
    AN : in STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \estado_reg[1]_0\ : in STD_LOGIC;
    \estado_reg[1]_1\ : in STD_LOGIC;
    \estado_reg[1]_2\ : in STD_LOGIC;
    sig_estado : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    EN_IBUF : in STD_LOGIC
  );
end FSM_psw;

architecture STRUCTURE of FSM_psw is
  signal \^estado\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \estado[1]_i_1_n_0\ : STD_LOGIC;
  signal \estado[1]_i_2_n_0\ : STD_LOGIC;
begin
  estado(2 downto 0) <= \^estado\(2 downto 0);
\estado[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040004040404"
    )
        port map (
      I0 => \^estado\(0),
      I1 => \^estado\(1),
      I2 => \^estado\(2),
      I3 => Q(0),
      I4 => Q(1),
      I5 => Q(2),
      O => \estado_reg[0]_0\
    );
\estado[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E2000000"
    )
        port map (
      I0 => \^estado\(1),
      I1 => sig_estado,
      I2 => \estado[1]_i_2_n_0\,
      I3 => reset_IBUF,
      I4 => EN_IBUF,
      O => \estado[1]_i_1_n_0\
    );
\estado[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000010000100000"
    )
        port map (
      I0 => \^estado\(2),
      I1 => \estado_reg[1]_0\,
      I2 => \estado_reg[1]_1\,
      I3 => \^estado\(0),
      I4 => \^estado\(1),
      I5 => \estado_reg[1]_2\,
      O => \estado[1]_i_2_n_0\
    );
\estado_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \estado_reg[0]_1\,
      Q => \^estado\(0),
      R => '0'
    );
\estado_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \estado[1]_i_1_n_0\,
      Q => \^estado\(1),
      R => '0'
    );
\estado_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \estado_reg[2]_0\,
      Q => \^estado\(2),
      R => '0'
    );
salidaOK_OBUF_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \^estado\(2),
      I1 => \^estado\(1),
      I2 => \^estado\(0),
      O => salidaOK_OBUF
    );
\segmentMSG_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF02FF020F2F0F0F"
    )
        port map (
      I0 => \^estado\(2),
      I1 => \^estado\(1),
      I2 => AN(2),
      I3 => AN(0),
      I4 => \^estado\(0),
      I5 => AN(1),
      O => segmentMSG_OBUF(1)
    );
\segmentMSG_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"02020202DF2FDF0F"
    )
        port map (
      I0 => \^estado\(2),
      I1 => \^estado\(1),
      I2 => AN(2),
      I3 => AN(0),
      I4 => \^estado\(0),
      I5 => AN(1),
      O => segmentMSG_OBUF(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    SYNC_OUT : in STD_LOGIC
  );
end debouncer;

architecture STRUCTURE of debouncer is
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \count_time[19]_i_1_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_3_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_4_n_0\ : STD_LOGIC;
  signal count_time_reg : STD_LOGIC_VECTOR ( 0 to 19 );
  signal \count_time_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_1\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_2\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_3\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_4\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_5\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_6\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2_n_7\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal reg_i_1_n_0 : STD_LOGIC;
  signal reg_i_2_n_0 : STD_LOGIC;
  signal reg_i_3_n_0 : STD_LOGIC;
  signal reg_i_4_n_0 : STD_LOGIC;
  signal reg_i_5_n_0 : STD_LOGIC;
  signal reg_i_6_n_0 : STD_LOGIC;
  signal reg_i_7_n_0 : STD_LOGIC;
  signal \NLW_count_time_reg[3]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \count_time_reg[11]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[15]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[19]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[3]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[7]_i_1\ : label is 11;
begin
  D(0) <= \^d\(0);
\count_time[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFBFFF"
    )
        port map (
      I0 => \count_time[19]_i_3_n_0\,
      I1 => count_time_reg(10),
      I2 => count_time_reg(5),
      I3 => count_time_reg(1),
      I4 => count_time_reg(15),
      I5 => reg_i_7_n_0,
      O => \count_time[19]_i_1_n_0\
    );
\count_time[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => count_time_reg(12),
      I1 => count_time_reg(6),
      I2 => count_time_reg(9),
      I3 => count_time_reg(2),
      I4 => reg_i_4_n_0,
      I5 => reg_i_5_n_0,
      O => \count_time[19]_i_3_n_0\
    );
\count_time[19]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_time_reg(19),
      O => \count_time[19]_i_4_n_0\
    );
\count_time_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1_n_4\,
      Q => count_time_reg(0)
    );
\count_time_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1_n_6\,
      Q => count_time_reg(10)
    );
\count_time_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1_n_7\,
      Q => count_time_reg(11)
    );
\count_time_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[15]_i_1_n_0\,
      CO(3) => \count_time_reg[11]_i_1_n_0\,
      CO(2) => \count_time_reg[11]_i_1_n_1\,
      CO(1) => \count_time_reg[11]_i_1_n_2\,
      CO(0) => \count_time_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[11]_i_1_n_4\,
      O(2) => \count_time_reg[11]_i_1_n_5\,
      O(1) => \count_time_reg[11]_i_1_n_6\,
      O(0) => \count_time_reg[11]_i_1_n_7\,
      S(3) => count_time_reg(8),
      S(2) => count_time_reg(9),
      S(1) => count_time_reg(10),
      S(0) => count_time_reg(11)
    );
\count_time_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1_n_4\,
      Q => count_time_reg(12)
    );
\count_time_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1_n_5\,
      Q => count_time_reg(13)
    );
\count_time_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1_n_6\,
      Q => count_time_reg(14)
    );
\count_time_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1_n_7\,
      Q => count_time_reg(15)
    );
\count_time_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[19]_i_2_n_0\,
      CO(3) => \count_time_reg[15]_i_1_n_0\,
      CO(2) => \count_time_reg[15]_i_1_n_1\,
      CO(1) => \count_time_reg[15]_i_1_n_2\,
      CO(0) => \count_time_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[15]_i_1_n_4\,
      O(2) => \count_time_reg[15]_i_1_n_5\,
      O(1) => \count_time_reg[15]_i_1_n_6\,
      O(0) => \count_time_reg[15]_i_1_n_7\,
      S(3) => count_time_reg(12),
      S(2) => count_time_reg(13),
      S(1) => count_time_reg(14),
      S(0) => count_time_reg(15)
    );
\count_time_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2_n_4\,
      Q => count_time_reg(16)
    );
\count_time_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2_n_5\,
      Q => count_time_reg(17)
    );
\count_time_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2_n_6\,
      Q => count_time_reg(18)
    );
\count_time_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2_n_7\,
      Q => count_time_reg(19)
    );
\count_time_reg[19]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_time_reg[19]_i_2_n_0\,
      CO(2) => \count_time_reg[19]_i_2_n_1\,
      CO(1) => \count_time_reg[19]_i_2_n_2\,
      CO(0) => \count_time_reg[19]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_time_reg[19]_i_2_n_4\,
      O(2) => \count_time_reg[19]_i_2_n_5\,
      O(1) => \count_time_reg[19]_i_2_n_6\,
      O(0) => \count_time_reg[19]_i_2_n_7\,
      S(3) => count_time_reg(16),
      S(2) => count_time_reg(17),
      S(1) => count_time_reg(18),
      S(0) => \count_time[19]_i_4_n_0\
    );
\count_time_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1_n_5\,
      Q => count_time_reg(1)
    );
\count_time_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1_n_6\,
      Q => count_time_reg(2)
    );
\count_time_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1_n_7\,
      Q => count_time_reg(3)
    );
\count_time_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[7]_i_1_n_0\,
      CO(3) => \NLW_count_time_reg[3]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \count_time_reg[3]_i_1_n_1\,
      CO(1) => \count_time_reg[3]_i_1_n_2\,
      CO(0) => \count_time_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[3]_i_1_n_4\,
      O(2) => \count_time_reg[3]_i_1_n_5\,
      O(1) => \count_time_reg[3]_i_1_n_6\,
      O(0) => \count_time_reg[3]_i_1_n_7\,
      S(3) => count_time_reg(0),
      S(2) => count_time_reg(1),
      S(1) => count_time_reg(2),
      S(0) => count_time_reg(3)
    );
\count_time_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1_n_4\,
      Q => count_time_reg(4)
    );
\count_time_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1_n_5\,
      Q => count_time_reg(5)
    );
\count_time_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1_n_6\,
      Q => count_time_reg(6)
    );
\count_time_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1_n_7\,
      Q => count_time_reg(7)
    );
\count_time_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[11]_i_1_n_0\,
      CO(3) => \count_time_reg[7]_i_1_n_0\,
      CO(2) => \count_time_reg[7]_i_1_n_1\,
      CO(1) => \count_time_reg[7]_i_1_n_2\,
      CO(0) => \count_time_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[7]_i_1_n_4\,
      O(2) => \count_time_reg[7]_i_1_n_5\,
      O(1) => \count_time_reg[7]_i_1_n_6\,
      O(0) => \count_time_reg[7]_i_1_n_7\,
      S(3) => count_time_reg(4),
      S(2) => count_time_reg(5),
      S(1) => count_time_reg(6),
      S(0) => count_time_reg(7)
    );
\count_time_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1_n_4\,
      Q => count_time_reg(8)
    );
\count_time_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1_n_5\,
      Q => count_time_reg(9)
    );
reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => reg_i_3_n_0,
      I1 => reg_i_4_n_0,
      I2 => reg_i_5_n_0,
      I3 => reg_i_6_n_0,
      I4 => reg_i_7_n_0,
      I5 => \^d\(0),
      O => reg_i_1_n_0
    );
reg_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => SYNC_OUT,
      O => reg_i_2_n_0
    );
reg_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(2),
      I1 => count_time_reg(9),
      I2 => count_time_reg(6),
      I3 => count_time_reg(12),
      O => reg_i_3_n_0
    );
reg_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(3),
      I1 => count_time_reg(8),
      I2 => count_time_reg(11),
      I3 => count_time_reg(14),
      O => reg_i_4_n_0
    );
reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(13),
      I1 => count_time_reg(17),
      I2 => count_time_reg(7),
      I3 => count_time_reg(18),
      O => reg_i_5_n_0
    );
reg_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => count_time_reg(15),
      I1 => count_time_reg(1),
      I2 => count_time_reg(5),
      I3 => count_time_reg(10),
      O => reg_i_6_n_0
    );
reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(0),
      I1 => count_time_reg(16),
      I2 => count_time_reg(4),
      I3 => count_time_reg(19),
      O => reg_i_7_n_0
    );
reg_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reg_i_2_n_0,
      D => reg_i_1_n_0,
      Q => \^d\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer_10 is
  port (
    reg_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    \count_time_reg[0]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of debouncer_10 : entity is "debouncer";
end debouncer_10;

architecture STRUCTURE of debouncer_10 is
  signal \count_time[19]_i_1__2_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_3__2_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_4__2_n_0\ : STD_LOGIC;
  signal count_time_reg : STD_LOGIC_VECTOR ( 0 to 19 );
  signal \count_time_reg[11]_i_1__2_n_0\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__2_n_1\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__2_n_2\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__2_n_3\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__2_n_4\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__2_n_5\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__2_n_6\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__2_n_7\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_0\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_1\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_2\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_3\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_4\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_5\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_6\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__2_n_7\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_0\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_1\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_2\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_3\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_4\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_5\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_6\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__2_n_7\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__2_n_1\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__2_n_2\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__2_n_3\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__2_n_4\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__2_n_5\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__2_n_6\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__2_n_7\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_0\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_1\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_2\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_3\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_4\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_5\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_6\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__2_n_7\ : STD_LOGIC;
  signal reg_i_1_n_0 : STD_LOGIC;
  signal reg_i_2_n_0 : STD_LOGIC;
  signal \reg_i_3__2_n_0\ : STD_LOGIC;
  signal \reg_i_4__2_n_0\ : STD_LOGIC;
  signal \reg_i_5__2_n_0\ : STD_LOGIC;
  signal \reg_i_6__2_n_0\ : STD_LOGIC;
  signal \reg_i_7__2_n_0\ : STD_LOGIC;
  signal \^reg_reg_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_count_time_reg[3]_i_1__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \count_time_reg[11]_i_1__2\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[15]_i_1__2\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[19]_i_2__2\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[3]_i_1__2\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[7]_i_1__2\ : label is 11;
begin
  reg_reg_0(0) <= \^reg_reg_0\(0);
\count_time[19]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFBFFF"
    )
        port map (
      I0 => \count_time[19]_i_3__2_n_0\,
      I1 => count_time_reg(10),
      I2 => count_time_reg(5),
      I3 => count_time_reg(1),
      I4 => count_time_reg(15),
      I5 => \reg_i_7__2_n_0\,
      O => \count_time[19]_i_1__2_n_0\
    );
\count_time[19]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => count_time_reg(12),
      I1 => count_time_reg(6),
      I2 => count_time_reg(9),
      I3 => count_time_reg(2),
      I4 => \reg_i_4__2_n_0\,
      I5 => \reg_i_5__2_n_0\,
      O => \count_time[19]_i_3__2_n_0\
    );
\count_time[19]_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_time_reg(19),
      O => \count_time[19]_i_4__2_n_0\
    );
\count_time_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__2_n_4\,
      Q => count_time_reg(0)
    );
\count_time_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__2_n_6\,
      Q => count_time_reg(10)
    );
\count_time_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__2_n_7\,
      Q => count_time_reg(11)
    );
\count_time_reg[11]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[15]_i_1__2_n_0\,
      CO(3) => \count_time_reg[11]_i_1__2_n_0\,
      CO(2) => \count_time_reg[11]_i_1__2_n_1\,
      CO(1) => \count_time_reg[11]_i_1__2_n_2\,
      CO(0) => \count_time_reg[11]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[11]_i_1__2_n_4\,
      O(2) => \count_time_reg[11]_i_1__2_n_5\,
      O(1) => \count_time_reg[11]_i_1__2_n_6\,
      O(0) => \count_time_reg[11]_i_1__2_n_7\,
      S(3) => count_time_reg(8),
      S(2) => count_time_reg(9),
      S(1) => count_time_reg(10),
      S(0) => count_time_reg(11)
    );
\count_time_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__2_n_4\,
      Q => count_time_reg(12)
    );
\count_time_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__2_n_5\,
      Q => count_time_reg(13)
    );
\count_time_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__2_n_6\,
      Q => count_time_reg(14)
    );
\count_time_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__2_n_7\,
      Q => count_time_reg(15)
    );
\count_time_reg[15]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[19]_i_2__2_n_0\,
      CO(3) => \count_time_reg[15]_i_1__2_n_0\,
      CO(2) => \count_time_reg[15]_i_1__2_n_1\,
      CO(1) => \count_time_reg[15]_i_1__2_n_2\,
      CO(0) => \count_time_reg[15]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[15]_i_1__2_n_4\,
      O(2) => \count_time_reg[15]_i_1__2_n_5\,
      O(1) => \count_time_reg[15]_i_1__2_n_6\,
      O(0) => \count_time_reg[15]_i_1__2_n_7\,
      S(3) => count_time_reg(12),
      S(2) => count_time_reg(13),
      S(1) => count_time_reg(14),
      S(0) => count_time_reg(15)
    );
\count_time_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__2_n_4\,
      Q => count_time_reg(16)
    );
\count_time_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__2_n_5\,
      Q => count_time_reg(17)
    );
\count_time_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__2_n_6\,
      Q => count_time_reg(18)
    );
\count_time_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__2_n_7\,
      Q => count_time_reg(19)
    );
\count_time_reg[19]_i_2__2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_time_reg[19]_i_2__2_n_0\,
      CO(2) => \count_time_reg[19]_i_2__2_n_1\,
      CO(1) => \count_time_reg[19]_i_2__2_n_2\,
      CO(0) => \count_time_reg[19]_i_2__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_time_reg[19]_i_2__2_n_4\,
      O(2) => \count_time_reg[19]_i_2__2_n_5\,
      O(1) => \count_time_reg[19]_i_2__2_n_6\,
      O(0) => \count_time_reg[19]_i_2__2_n_7\,
      S(3) => count_time_reg(16),
      S(2) => count_time_reg(17),
      S(1) => count_time_reg(18),
      S(0) => \count_time[19]_i_4__2_n_0\
    );
\count_time_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__2_n_5\,
      Q => count_time_reg(1)
    );
\count_time_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__2_n_6\,
      Q => count_time_reg(2)
    );
\count_time_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__2_n_7\,
      Q => count_time_reg(3)
    );
\count_time_reg[3]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[7]_i_1__2_n_0\,
      CO(3) => \NLW_count_time_reg[3]_i_1__2_CO_UNCONNECTED\(3),
      CO(2) => \count_time_reg[3]_i_1__2_n_1\,
      CO(1) => \count_time_reg[3]_i_1__2_n_2\,
      CO(0) => \count_time_reg[3]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[3]_i_1__2_n_4\,
      O(2) => \count_time_reg[3]_i_1__2_n_5\,
      O(1) => \count_time_reg[3]_i_1__2_n_6\,
      O(0) => \count_time_reg[3]_i_1__2_n_7\,
      S(3) => count_time_reg(0),
      S(2) => count_time_reg(1),
      S(1) => count_time_reg(2),
      S(0) => count_time_reg(3)
    );
\count_time_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__2_n_4\,
      Q => count_time_reg(4)
    );
\count_time_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__2_n_5\,
      Q => count_time_reg(5)
    );
\count_time_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__2_n_6\,
      Q => count_time_reg(6)
    );
\count_time_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__2_n_7\,
      Q => count_time_reg(7)
    );
\count_time_reg[7]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[11]_i_1__2_n_0\,
      CO(3) => \count_time_reg[7]_i_1__2_n_0\,
      CO(2) => \count_time_reg[7]_i_1__2_n_1\,
      CO(1) => \count_time_reg[7]_i_1__2_n_2\,
      CO(0) => \count_time_reg[7]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[7]_i_1__2_n_4\,
      O(2) => \count_time_reg[7]_i_1__2_n_5\,
      O(1) => \count_time_reg[7]_i_1__2_n_6\,
      O(0) => \count_time_reg[7]_i_1__2_n_7\,
      S(3) => count_time_reg(4),
      S(2) => count_time_reg(5),
      S(1) => count_time_reg(6),
      S(0) => count_time_reg(7)
    );
\count_time_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__2_n_4\,
      Q => count_time_reg(8)
    );
\count_time_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__2_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__2_n_5\,
      Q => count_time_reg(9)
    );
reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => \reg_i_3__2_n_0\,
      I1 => \reg_i_4__2_n_0\,
      I2 => \reg_i_5__2_n_0\,
      I3 => \reg_i_6__2_n_0\,
      I4 => \reg_i_7__2_n_0\,
      I5 => \^reg_reg_0\(0),
      O => reg_i_1_n_0
    );
reg_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count_time_reg[0]_0\,
      O => reg_i_2_n_0
    );
\reg_i_3__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(2),
      I1 => count_time_reg(9),
      I2 => count_time_reg(6),
      I3 => count_time_reg(12),
      O => \reg_i_3__2_n_0\
    );
\reg_i_4__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(3),
      I1 => count_time_reg(8),
      I2 => count_time_reg(11),
      I3 => count_time_reg(14),
      O => \reg_i_4__2_n_0\
    );
\reg_i_5__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(13),
      I1 => count_time_reg(17),
      I2 => count_time_reg(7),
      I3 => count_time_reg(18),
      O => \reg_i_5__2_n_0\
    );
\reg_i_6__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => count_time_reg(15),
      I1 => count_time_reg(1),
      I2 => count_time_reg(5),
      I3 => count_time_reg(10),
      O => \reg_i_6__2_n_0\
    );
\reg_i_7__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(0),
      I1 => count_time_reg(16),
      I2 => count_time_reg(4),
      I3 => count_time_reg(19),
      O => \reg_i_7__2_n_0\
    );
reg_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reg_i_2_n_0,
      D => reg_i_1_n_0,
      Q => \^reg_reg_0\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer_11 is
  port (
    reg_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    \count_time_reg[0]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of debouncer_11 : entity is "debouncer";
end debouncer_11;

architecture STRUCTURE of debouncer_11 is
  signal \count_time[19]_i_1__3_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_3__3_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_4__3_n_0\ : STD_LOGIC;
  signal count_time_reg : STD_LOGIC_VECTOR ( 0 to 19 );
  signal \count_time_reg[11]_i_1__3_n_0\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__3_n_1\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__3_n_2\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__3_n_3\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__3_n_4\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__3_n_5\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__3_n_6\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__3_n_7\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_0\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_1\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_2\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_3\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_4\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_5\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_6\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__3_n_7\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_0\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_1\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_2\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_3\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_4\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_5\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_6\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__3_n_7\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__3_n_1\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__3_n_2\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__3_n_3\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__3_n_4\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__3_n_5\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__3_n_6\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__3_n_7\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_0\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_1\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_2\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_3\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_4\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_5\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_6\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__3_n_7\ : STD_LOGIC;
  signal reg_i_1_n_0 : STD_LOGIC;
  signal reg_i_2_n_0 : STD_LOGIC;
  signal \reg_i_3__3_n_0\ : STD_LOGIC;
  signal \reg_i_4__3_n_0\ : STD_LOGIC;
  signal \reg_i_5__3_n_0\ : STD_LOGIC;
  signal \reg_i_6__3_n_0\ : STD_LOGIC;
  signal \reg_i_7__3_n_0\ : STD_LOGIC;
  signal \^reg_reg_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_count_time_reg[3]_i_1__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \count_time_reg[11]_i_1__3\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[15]_i_1__3\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[19]_i_2__3\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[3]_i_1__3\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[7]_i_1__3\ : label is 11;
begin
  reg_reg_0(0) <= \^reg_reg_0\(0);
\count_time[19]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFBFFF"
    )
        port map (
      I0 => \count_time[19]_i_3__3_n_0\,
      I1 => count_time_reg(10),
      I2 => count_time_reg(5),
      I3 => count_time_reg(1),
      I4 => count_time_reg(15),
      I5 => \reg_i_7__3_n_0\,
      O => \count_time[19]_i_1__3_n_0\
    );
\count_time[19]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => count_time_reg(12),
      I1 => count_time_reg(6),
      I2 => count_time_reg(9),
      I3 => count_time_reg(2),
      I4 => \reg_i_4__3_n_0\,
      I5 => \reg_i_5__3_n_0\,
      O => \count_time[19]_i_3__3_n_0\
    );
\count_time[19]_i_4__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_time_reg(19),
      O => \count_time[19]_i_4__3_n_0\
    );
\count_time_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__3_n_4\,
      Q => count_time_reg(0)
    );
\count_time_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__3_n_6\,
      Q => count_time_reg(10)
    );
\count_time_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__3_n_7\,
      Q => count_time_reg(11)
    );
\count_time_reg[11]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[15]_i_1__3_n_0\,
      CO(3) => \count_time_reg[11]_i_1__3_n_0\,
      CO(2) => \count_time_reg[11]_i_1__3_n_1\,
      CO(1) => \count_time_reg[11]_i_1__3_n_2\,
      CO(0) => \count_time_reg[11]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[11]_i_1__3_n_4\,
      O(2) => \count_time_reg[11]_i_1__3_n_5\,
      O(1) => \count_time_reg[11]_i_1__3_n_6\,
      O(0) => \count_time_reg[11]_i_1__3_n_7\,
      S(3) => count_time_reg(8),
      S(2) => count_time_reg(9),
      S(1) => count_time_reg(10),
      S(0) => count_time_reg(11)
    );
\count_time_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__3_n_4\,
      Q => count_time_reg(12)
    );
\count_time_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__3_n_5\,
      Q => count_time_reg(13)
    );
\count_time_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__3_n_6\,
      Q => count_time_reg(14)
    );
\count_time_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__3_n_7\,
      Q => count_time_reg(15)
    );
\count_time_reg[15]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[19]_i_2__3_n_0\,
      CO(3) => \count_time_reg[15]_i_1__3_n_0\,
      CO(2) => \count_time_reg[15]_i_1__3_n_1\,
      CO(1) => \count_time_reg[15]_i_1__3_n_2\,
      CO(0) => \count_time_reg[15]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[15]_i_1__3_n_4\,
      O(2) => \count_time_reg[15]_i_1__3_n_5\,
      O(1) => \count_time_reg[15]_i_1__3_n_6\,
      O(0) => \count_time_reg[15]_i_1__3_n_7\,
      S(3) => count_time_reg(12),
      S(2) => count_time_reg(13),
      S(1) => count_time_reg(14),
      S(0) => count_time_reg(15)
    );
\count_time_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__3_n_4\,
      Q => count_time_reg(16)
    );
\count_time_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__3_n_5\,
      Q => count_time_reg(17)
    );
\count_time_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__3_n_6\,
      Q => count_time_reg(18)
    );
\count_time_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__3_n_7\,
      Q => count_time_reg(19)
    );
\count_time_reg[19]_i_2__3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_time_reg[19]_i_2__3_n_0\,
      CO(2) => \count_time_reg[19]_i_2__3_n_1\,
      CO(1) => \count_time_reg[19]_i_2__3_n_2\,
      CO(0) => \count_time_reg[19]_i_2__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_time_reg[19]_i_2__3_n_4\,
      O(2) => \count_time_reg[19]_i_2__3_n_5\,
      O(1) => \count_time_reg[19]_i_2__3_n_6\,
      O(0) => \count_time_reg[19]_i_2__3_n_7\,
      S(3) => count_time_reg(16),
      S(2) => count_time_reg(17),
      S(1) => count_time_reg(18),
      S(0) => \count_time[19]_i_4__3_n_0\
    );
\count_time_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__3_n_5\,
      Q => count_time_reg(1)
    );
\count_time_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__3_n_6\,
      Q => count_time_reg(2)
    );
\count_time_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__3_n_7\,
      Q => count_time_reg(3)
    );
\count_time_reg[3]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[7]_i_1__3_n_0\,
      CO(3) => \NLW_count_time_reg[3]_i_1__3_CO_UNCONNECTED\(3),
      CO(2) => \count_time_reg[3]_i_1__3_n_1\,
      CO(1) => \count_time_reg[3]_i_1__3_n_2\,
      CO(0) => \count_time_reg[3]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[3]_i_1__3_n_4\,
      O(2) => \count_time_reg[3]_i_1__3_n_5\,
      O(1) => \count_time_reg[3]_i_1__3_n_6\,
      O(0) => \count_time_reg[3]_i_1__3_n_7\,
      S(3) => count_time_reg(0),
      S(2) => count_time_reg(1),
      S(1) => count_time_reg(2),
      S(0) => count_time_reg(3)
    );
\count_time_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__3_n_4\,
      Q => count_time_reg(4)
    );
\count_time_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__3_n_5\,
      Q => count_time_reg(5)
    );
\count_time_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__3_n_6\,
      Q => count_time_reg(6)
    );
\count_time_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__3_n_7\,
      Q => count_time_reg(7)
    );
\count_time_reg[7]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[11]_i_1__3_n_0\,
      CO(3) => \count_time_reg[7]_i_1__3_n_0\,
      CO(2) => \count_time_reg[7]_i_1__3_n_1\,
      CO(1) => \count_time_reg[7]_i_1__3_n_2\,
      CO(0) => \count_time_reg[7]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[7]_i_1__3_n_4\,
      O(2) => \count_time_reg[7]_i_1__3_n_5\,
      O(1) => \count_time_reg[7]_i_1__3_n_6\,
      O(0) => \count_time_reg[7]_i_1__3_n_7\,
      S(3) => count_time_reg(4),
      S(2) => count_time_reg(5),
      S(1) => count_time_reg(6),
      S(0) => count_time_reg(7)
    );
\count_time_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__3_n_4\,
      Q => count_time_reg(8)
    );
\count_time_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__3_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__3_n_5\,
      Q => count_time_reg(9)
    );
reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => \reg_i_3__3_n_0\,
      I1 => \reg_i_4__3_n_0\,
      I2 => \reg_i_5__3_n_0\,
      I3 => \reg_i_6__3_n_0\,
      I4 => \reg_i_7__3_n_0\,
      I5 => \^reg_reg_0\(0),
      O => reg_i_1_n_0
    );
reg_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count_time_reg[0]_0\,
      O => reg_i_2_n_0
    );
\reg_i_3__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(2),
      I1 => count_time_reg(9),
      I2 => count_time_reg(6),
      I3 => count_time_reg(12),
      O => \reg_i_3__3_n_0\
    );
\reg_i_4__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(3),
      I1 => count_time_reg(8),
      I2 => count_time_reg(11),
      I3 => count_time_reg(14),
      O => \reg_i_4__3_n_0\
    );
\reg_i_5__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(13),
      I1 => count_time_reg(17),
      I2 => count_time_reg(7),
      I3 => count_time_reg(18),
      O => \reg_i_5__3_n_0\
    );
\reg_i_6__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => count_time_reg(15),
      I1 => count_time_reg(1),
      I2 => count_time_reg(5),
      I3 => count_time_reg(10),
      O => \reg_i_6__3_n_0\
    );
\reg_i_7__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(0),
      I1 => count_time_reg(16),
      I2 => count_time_reg(4),
      I3 => count_time_reg(19),
      O => \reg_i_7__3_n_0\
    );
reg_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reg_i_2_n_0,
      D => reg_i_1_n_0,
      Q => \^reg_reg_0\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer_8 is
  port (
    reg_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    \count_time_reg[0]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of debouncer_8 : entity is "debouncer";
end debouncer_8;

architecture STRUCTURE of debouncer_8 is
  signal \count_time[19]_i_1__0_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_3__0_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_4__0_n_0\ : STD_LOGIC;
  signal count_time_reg : STD_LOGIC_VECTOR ( 0 to 19 );
  signal \count_time_reg[11]_i_1__0_n_0\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__0_n_1\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__0_n_2\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__0_n_3\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__0_n_4\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__0_n_5\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__0_n_6\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__0_n_7\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_0\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_1\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_2\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_3\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_4\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_5\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_6\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__0_n_7\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_0\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_1\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_2\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_3\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_4\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_5\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_6\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__0_n_7\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__0_n_1\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__0_n_2\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__0_n_3\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__0_n_4\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__0_n_5\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__0_n_6\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__0_n_7\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_1\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_2\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_3\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_4\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_5\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_6\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__0_n_7\ : STD_LOGIC;
  signal reg_i_1_n_0 : STD_LOGIC;
  signal reg_i_2_n_0 : STD_LOGIC;
  signal \reg_i_3__0_n_0\ : STD_LOGIC;
  signal \reg_i_4__0_n_0\ : STD_LOGIC;
  signal \reg_i_5__0_n_0\ : STD_LOGIC;
  signal \reg_i_6__0_n_0\ : STD_LOGIC;
  signal \reg_i_7__0_n_0\ : STD_LOGIC;
  signal \^reg_reg_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_count_time_reg[3]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \count_time_reg[11]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[15]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[19]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[3]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[7]_i_1__0\ : label is 11;
begin
  reg_reg_0(0) <= \^reg_reg_0\(0);
\count_time[19]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFBFFF"
    )
        port map (
      I0 => \count_time[19]_i_3__0_n_0\,
      I1 => count_time_reg(10),
      I2 => count_time_reg(5),
      I3 => count_time_reg(1),
      I4 => count_time_reg(15),
      I5 => \reg_i_7__0_n_0\,
      O => \count_time[19]_i_1__0_n_0\
    );
\count_time[19]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => count_time_reg(12),
      I1 => count_time_reg(6),
      I2 => count_time_reg(9),
      I3 => count_time_reg(2),
      I4 => \reg_i_4__0_n_0\,
      I5 => \reg_i_5__0_n_0\,
      O => \count_time[19]_i_3__0_n_0\
    );
\count_time[19]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_time_reg(19),
      O => \count_time[19]_i_4__0_n_0\
    );
\count_time_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__0_n_4\,
      Q => count_time_reg(0)
    );
\count_time_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__0_n_6\,
      Q => count_time_reg(10)
    );
\count_time_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__0_n_7\,
      Q => count_time_reg(11)
    );
\count_time_reg[11]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[15]_i_1__0_n_0\,
      CO(3) => \count_time_reg[11]_i_1__0_n_0\,
      CO(2) => \count_time_reg[11]_i_1__0_n_1\,
      CO(1) => \count_time_reg[11]_i_1__0_n_2\,
      CO(0) => \count_time_reg[11]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[11]_i_1__0_n_4\,
      O(2) => \count_time_reg[11]_i_1__0_n_5\,
      O(1) => \count_time_reg[11]_i_1__0_n_6\,
      O(0) => \count_time_reg[11]_i_1__0_n_7\,
      S(3) => count_time_reg(8),
      S(2) => count_time_reg(9),
      S(1) => count_time_reg(10),
      S(0) => count_time_reg(11)
    );
\count_time_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__0_n_4\,
      Q => count_time_reg(12)
    );
\count_time_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__0_n_5\,
      Q => count_time_reg(13)
    );
\count_time_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__0_n_6\,
      Q => count_time_reg(14)
    );
\count_time_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__0_n_7\,
      Q => count_time_reg(15)
    );
\count_time_reg[15]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[19]_i_2__0_n_0\,
      CO(3) => \count_time_reg[15]_i_1__0_n_0\,
      CO(2) => \count_time_reg[15]_i_1__0_n_1\,
      CO(1) => \count_time_reg[15]_i_1__0_n_2\,
      CO(0) => \count_time_reg[15]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[15]_i_1__0_n_4\,
      O(2) => \count_time_reg[15]_i_1__0_n_5\,
      O(1) => \count_time_reg[15]_i_1__0_n_6\,
      O(0) => \count_time_reg[15]_i_1__0_n_7\,
      S(3) => count_time_reg(12),
      S(2) => count_time_reg(13),
      S(1) => count_time_reg(14),
      S(0) => count_time_reg(15)
    );
\count_time_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__0_n_4\,
      Q => count_time_reg(16)
    );
\count_time_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__0_n_5\,
      Q => count_time_reg(17)
    );
\count_time_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__0_n_6\,
      Q => count_time_reg(18)
    );
\count_time_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__0_n_7\,
      Q => count_time_reg(19)
    );
\count_time_reg[19]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_time_reg[19]_i_2__0_n_0\,
      CO(2) => \count_time_reg[19]_i_2__0_n_1\,
      CO(1) => \count_time_reg[19]_i_2__0_n_2\,
      CO(0) => \count_time_reg[19]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_time_reg[19]_i_2__0_n_4\,
      O(2) => \count_time_reg[19]_i_2__0_n_5\,
      O(1) => \count_time_reg[19]_i_2__0_n_6\,
      O(0) => \count_time_reg[19]_i_2__0_n_7\,
      S(3) => count_time_reg(16),
      S(2) => count_time_reg(17),
      S(1) => count_time_reg(18),
      S(0) => \count_time[19]_i_4__0_n_0\
    );
\count_time_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__0_n_5\,
      Q => count_time_reg(1)
    );
\count_time_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__0_n_6\,
      Q => count_time_reg(2)
    );
\count_time_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__0_n_7\,
      Q => count_time_reg(3)
    );
\count_time_reg[3]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[7]_i_1__0_n_0\,
      CO(3) => \NLW_count_time_reg[3]_i_1__0_CO_UNCONNECTED\(3),
      CO(2) => \count_time_reg[3]_i_1__0_n_1\,
      CO(1) => \count_time_reg[3]_i_1__0_n_2\,
      CO(0) => \count_time_reg[3]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[3]_i_1__0_n_4\,
      O(2) => \count_time_reg[3]_i_1__0_n_5\,
      O(1) => \count_time_reg[3]_i_1__0_n_6\,
      O(0) => \count_time_reg[3]_i_1__0_n_7\,
      S(3) => count_time_reg(0),
      S(2) => count_time_reg(1),
      S(1) => count_time_reg(2),
      S(0) => count_time_reg(3)
    );
\count_time_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__0_n_4\,
      Q => count_time_reg(4)
    );
\count_time_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__0_n_5\,
      Q => count_time_reg(5)
    );
\count_time_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__0_n_6\,
      Q => count_time_reg(6)
    );
\count_time_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__0_n_7\,
      Q => count_time_reg(7)
    );
\count_time_reg[7]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[11]_i_1__0_n_0\,
      CO(3) => \count_time_reg[7]_i_1__0_n_0\,
      CO(2) => \count_time_reg[7]_i_1__0_n_1\,
      CO(1) => \count_time_reg[7]_i_1__0_n_2\,
      CO(0) => \count_time_reg[7]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[7]_i_1__0_n_4\,
      O(2) => \count_time_reg[7]_i_1__0_n_5\,
      O(1) => \count_time_reg[7]_i_1__0_n_6\,
      O(0) => \count_time_reg[7]_i_1__0_n_7\,
      S(3) => count_time_reg(4),
      S(2) => count_time_reg(5),
      S(1) => count_time_reg(6),
      S(0) => count_time_reg(7)
    );
\count_time_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__0_n_4\,
      Q => count_time_reg(8)
    );
\count_time_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__0_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__0_n_5\,
      Q => count_time_reg(9)
    );
reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => \reg_i_3__0_n_0\,
      I1 => \reg_i_4__0_n_0\,
      I2 => \reg_i_5__0_n_0\,
      I3 => \reg_i_6__0_n_0\,
      I4 => \reg_i_7__0_n_0\,
      I5 => \^reg_reg_0\(0),
      O => reg_i_1_n_0
    );
reg_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count_time_reg[0]_0\,
      O => reg_i_2_n_0
    );
\reg_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(2),
      I1 => count_time_reg(9),
      I2 => count_time_reg(6),
      I3 => count_time_reg(12),
      O => \reg_i_3__0_n_0\
    );
\reg_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(3),
      I1 => count_time_reg(8),
      I2 => count_time_reg(11),
      I3 => count_time_reg(14),
      O => \reg_i_4__0_n_0\
    );
\reg_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(13),
      I1 => count_time_reg(17),
      I2 => count_time_reg(7),
      I3 => count_time_reg(18),
      O => \reg_i_5__0_n_0\
    );
\reg_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => count_time_reg(15),
      I1 => count_time_reg(1),
      I2 => count_time_reg(5),
      I3 => count_time_reg(10),
      O => \reg_i_6__0_n_0\
    );
\reg_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(0),
      I1 => count_time_reg(16),
      I2 => count_time_reg(4),
      I3 => count_time_reg(19),
      O => \reg_i_7__0_n_0\
    );
reg_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reg_i_2_n_0,
      D => reg_i_1_n_0,
      Q => \^reg_reg_0\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debouncer_9 is
  port (
    reg_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    \count_time_reg[0]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of debouncer_9 : entity is "debouncer";
end debouncer_9;

architecture STRUCTURE of debouncer_9 is
  signal \count_time[19]_i_1__1_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_3__1_n_0\ : STD_LOGIC;
  signal \count_time[19]_i_4__1_n_0\ : STD_LOGIC;
  signal count_time_reg : STD_LOGIC_VECTOR ( 0 to 19 );
  signal \count_time_reg[11]_i_1__1_n_0\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__1_n_1\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__1_n_2\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__1_n_3\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__1_n_4\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__1_n_5\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__1_n_6\ : STD_LOGIC;
  signal \count_time_reg[11]_i_1__1_n_7\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_0\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_1\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_2\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_3\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_4\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_5\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_6\ : STD_LOGIC;
  signal \count_time_reg[15]_i_1__1_n_7\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_0\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_1\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_2\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_3\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_4\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_5\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_6\ : STD_LOGIC;
  signal \count_time_reg[19]_i_2__1_n_7\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__1_n_1\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__1_n_2\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__1_n_3\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__1_n_4\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__1_n_5\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__1_n_6\ : STD_LOGIC;
  signal \count_time_reg[3]_i_1__1_n_7\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_0\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_1\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_2\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_3\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_4\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_5\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_6\ : STD_LOGIC;
  signal \count_time_reg[7]_i_1__1_n_7\ : STD_LOGIC;
  signal reg_i_1_n_0 : STD_LOGIC;
  signal reg_i_2_n_0 : STD_LOGIC;
  signal \reg_i_3__1_n_0\ : STD_LOGIC;
  signal \reg_i_4__1_n_0\ : STD_LOGIC;
  signal \reg_i_5__1_n_0\ : STD_LOGIC;
  signal \reg_i_6__1_n_0\ : STD_LOGIC;
  signal \reg_i_7__1_n_0\ : STD_LOGIC;
  signal \^reg_reg_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_count_time_reg[3]_i_1__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \count_time_reg[11]_i_1__1\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[15]_i_1__1\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[19]_i_2__1\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[3]_i_1__1\ : label is 11;
  attribute ADDER_THRESHOLD of \count_time_reg[7]_i_1__1\ : label is 11;
begin
  reg_reg_0(0) <= \^reg_reg_0\(0);
\count_time[19]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFBFFF"
    )
        port map (
      I0 => \count_time[19]_i_3__1_n_0\,
      I1 => count_time_reg(10),
      I2 => count_time_reg(5),
      I3 => count_time_reg(1),
      I4 => count_time_reg(15),
      I5 => \reg_i_7__1_n_0\,
      O => \count_time[19]_i_1__1_n_0\
    );
\count_time[19]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => count_time_reg(12),
      I1 => count_time_reg(6),
      I2 => count_time_reg(9),
      I3 => count_time_reg(2),
      I4 => \reg_i_4__1_n_0\,
      I5 => \reg_i_5__1_n_0\,
      O => \count_time[19]_i_3__1_n_0\
    );
\count_time[19]_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_time_reg(19),
      O => \count_time[19]_i_4__1_n_0\
    );
\count_time_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__1_n_4\,
      Q => count_time_reg(0)
    );
\count_time_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__1_n_6\,
      Q => count_time_reg(10)
    );
\count_time_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__1_n_7\,
      Q => count_time_reg(11)
    );
\count_time_reg[11]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[15]_i_1__1_n_0\,
      CO(3) => \count_time_reg[11]_i_1__1_n_0\,
      CO(2) => \count_time_reg[11]_i_1__1_n_1\,
      CO(1) => \count_time_reg[11]_i_1__1_n_2\,
      CO(0) => \count_time_reg[11]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[11]_i_1__1_n_4\,
      O(2) => \count_time_reg[11]_i_1__1_n_5\,
      O(1) => \count_time_reg[11]_i_1__1_n_6\,
      O(0) => \count_time_reg[11]_i_1__1_n_7\,
      S(3) => count_time_reg(8),
      S(2) => count_time_reg(9),
      S(1) => count_time_reg(10),
      S(0) => count_time_reg(11)
    );
\count_time_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__1_n_4\,
      Q => count_time_reg(12)
    );
\count_time_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__1_n_5\,
      Q => count_time_reg(13)
    );
\count_time_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__1_n_6\,
      Q => count_time_reg(14)
    );
\count_time_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[15]_i_1__1_n_7\,
      Q => count_time_reg(15)
    );
\count_time_reg[15]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[19]_i_2__1_n_0\,
      CO(3) => \count_time_reg[15]_i_1__1_n_0\,
      CO(2) => \count_time_reg[15]_i_1__1_n_1\,
      CO(1) => \count_time_reg[15]_i_1__1_n_2\,
      CO(0) => \count_time_reg[15]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[15]_i_1__1_n_4\,
      O(2) => \count_time_reg[15]_i_1__1_n_5\,
      O(1) => \count_time_reg[15]_i_1__1_n_6\,
      O(0) => \count_time_reg[15]_i_1__1_n_7\,
      S(3) => count_time_reg(12),
      S(2) => count_time_reg(13),
      S(1) => count_time_reg(14),
      S(0) => count_time_reg(15)
    );
\count_time_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__1_n_4\,
      Q => count_time_reg(16)
    );
\count_time_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__1_n_5\,
      Q => count_time_reg(17)
    );
\count_time_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__1_n_6\,
      Q => count_time_reg(18)
    );
\count_time_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[19]_i_2__1_n_7\,
      Q => count_time_reg(19)
    );
\count_time_reg[19]_i_2__1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_time_reg[19]_i_2__1_n_0\,
      CO(2) => \count_time_reg[19]_i_2__1_n_1\,
      CO(1) => \count_time_reg[19]_i_2__1_n_2\,
      CO(0) => \count_time_reg[19]_i_2__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_time_reg[19]_i_2__1_n_4\,
      O(2) => \count_time_reg[19]_i_2__1_n_5\,
      O(1) => \count_time_reg[19]_i_2__1_n_6\,
      O(0) => \count_time_reg[19]_i_2__1_n_7\,
      S(3) => count_time_reg(16),
      S(2) => count_time_reg(17),
      S(1) => count_time_reg(18),
      S(0) => \count_time[19]_i_4__1_n_0\
    );
\count_time_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__1_n_5\,
      Q => count_time_reg(1)
    );
\count_time_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__1_n_6\,
      Q => count_time_reg(2)
    );
\count_time_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[3]_i_1__1_n_7\,
      Q => count_time_reg(3)
    );
\count_time_reg[3]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[7]_i_1__1_n_0\,
      CO(3) => \NLW_count_time_reg[3]_i_1__1_CO_UNCONNECTED\(3),
      CO(2) => \count_time_reg[3]_i_1__1_n_1\,
      CO(1) => \count_time_reg[3]_i_1__1_n_2\,
      CO(0) => \count_time_reg[3]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[3]_i_1__1_n_4\,
      O(2) => \count_time_reg[3]_i_1__1_n_5\,
      O(1) => \count_time_reg[3]_i_1__1_n_6\,
      O(0) => \count_time_reg[3]_i_1__1_n_7\,
      S(3) => count_time_reg(0),
      S(2) => count_time_reg(1),
      S(1) => count_time_reg(2),
      S(0) => count_time_reg(3)
    );
\count_time_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__1_n_4\,
      Q => count_time_reg(4)
    );
\count_time_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__1_n_5\,
      Q => count_time_reg(5)
    );
\count_time_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__1_n_6\,
      Q => count_time_reg(6)
    );
\count_time_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[7]_i_1__1_n_7\,
      Q => count_time_reg(7)
    );
\count_time_reg[7]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_time_reg[11]_i_1__1_n_0\,
      CO(3) => \count_time_reg[7]_i_1__1_n_0\,
      CO(2) => \count_time_reg[7]_i_1__1_n_1\,
      CO(1) => \count_time_reg[7]_i_1__1_n_2\,
      CO(0) => \count_time_reg[7]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_time_reg[7]_i_1__1_n_4\,
      O(2) => \count_time_reg[7]_i_1__1_n_5\,
      O(1) => \count_time_reg[7]_i_1__1_n_6\,
      O(0) => \count_time_reg[7]_i_1__1_n_7\,
      S(3) => count_time_reg(4),
      S(2) => count_time_reg(5),
      S(1) => count_time_reg(6),
      S(0) => count_time_reg(7)
    );
\count_time_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__1_n_4\,
      Q => count_time_reg(8)
    );
\count_time_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \count_time[19]_i_1__1_n_0\,
      CLR => reg_i_2_n_0,
      D => \count_time_reg[11]_i_1__1_n_5\,
      Q => count_time_reg(9)
    );
reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => \reg_i_3__1_n_0\,
      I1 => \reg_i_4__1_n_0\,
      I2 => \reg_i_5__1_n_0\,
      I3 => \reg_i_6__1_n_0\,
      I4 => \reg_i_7__1_n_0\,
      I5 => \^reg_reg_0\(0),
      O => reg_i_1_n_0
    );
reg_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count_time_reg[0]_0\,
      O => reg_i_2_n_0
    );
\reg_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(2),
      I1 => count_time_reg(9),
      I2 => count_time_reg(6),
      I3 => count_time_reg(12),
      O => \reg_i_3__1_n_0\
    );
\reg_i_4__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(3),
      I1 => count_time_reg(8),
      I2 => count_time_reg(11),
      I3 => count_time_reg(14),
      O => \reg_i_4__1_n_0\
    );
\reg_i_5__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(13),
      I1 => count_time_reg(17),
      I2 => count_time_reg(7),
      I3 => count_time_reg(18),
      O => \reg_i_5__1_n_0\
    );
\reg_i_6__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => count_time_reg(15),
      I1 => count_time_reg(1),
      I2 => count_time_reg(5),
      I3 => count_time_reg(10),
      O => \reg_i_6__1_n_0\
    );
\reg_i_7__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_time_reg(0),
      I1 => count_time_reg(16),
      I2 => count_time_reg(4),
      I3 => count_time_reg(19),
      O => \reg_i_7__1_n_0\
    );
reg_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reg_i_2_n_0,
      D => reg_i_1_n_0,
      Q => \^reg_reg_0\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edge_detector is
  port (
    \sreg_reg[2]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end edge_detector;

architecture STRUCTURE of edge_detector is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
\estado[2]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \sreg_reg[2]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \^q\(1),
      Q => \^q\(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edge_detector_4 is
  port (
    \estado_reg[0]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    estado : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \estado_reg[2]\ : in STD_LOGIC;
    \estado_reg[2]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edge_detector_4 : entity is "edge_detector";
end edge_detector_4;

architecture STRUCTURE of edge_detector_4 is
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
begin
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
\estado[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400000"
    )
        port map (
      I0 => \^sreg_reg[2]_0\,
      I1 => estado(0),
      I2 => estado(1),
      I3 => estado(2),
      I4 => \estado_reg[2]\,
      I5 => \estado_reg[2]_0\,
      O => \estado_reg[0]\
    );
\estado[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101010FF10101010"
    )
        port map (
      I0 => sreg(0),
      I1 => sreg(1),
      I2 => sreg(2),
      I3 => Q(0),
      I4 => Q(1),
      I5 => Q(2),
      O => \sreg_reg[0]_0\
    );
\estado[2]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      I2 => sreg(0),
      O => \^sreg_reg[2]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\(0),
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edge_detector_5 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edge_detector_5 : entity is "edge_detector";
end edge_detector_5;

architecture STRUCTURE of edge_detector_5 is
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
\estado[1]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      I2 => sreg(0),
      O => \sreg_reg[2]_0\
    );
\estado[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101010FF10101010"
    )
        port map (
      I0 => sreg(0),
      I1 => sreg(1),
      I2 => sreg(2),
      I3 => Q(0),
      I4 => Q(1),
      I5 => Q(2),
      O => \sreg_reg[0]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\(0),
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edge_detector_6 is
  port (
    \estado_reg[2]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    estado : in STD_LOGIC_VECTOR ( 0 to 0 );
    \estado_reg[0]\ : in STD_LOGIC;
    \estado_reg[0]_0\ : in STD_LOGIC;
    \estado_reg[0]_1\ : in STD_LOGIC;
    \estado_reg[0]_2\ : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edge_detector_6 : entity is "edge_detector";
end edge_detector_6;

architecture STRUCTURE of edge_detector_6 is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
\estado[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00200020F0000000"
    )
        port map (
      I0 => \^sreg_reg[2]_0\,
      I1 => estado(0),
      I2 => \estado_reg[0]\,
      I3 => \estado_reg[0]_0\,
      I4 => \estado_reg[0]_1\,
      I5 => \estado_reg[0]_2\,
      O => \estado_reg[2]\
    );
\estado[1]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \^sreg_reg[2]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg[0]_0\(0),
      Q => \^q\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \^q\(1),
      Q => \^q\(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edge_detector_7 is
  port (
    sig_estado : out STD_LOGIC;
    \estado_reg[0]\ : out STD_LOGIC;
    \estado_reg[2]\ : out STD_LOGIC;
    \estado_reg[0]_0\ : in STD_LOGIC;
    \estado_reg[0]_1\ : in STD_LOGIC;
    estado : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \estado_reg[0]_2\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    EN_IBUF : in STD_LOGIC;
    \estado_reg[2]_0\ : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edge_detector_7 : entity is "edge_detector";
end edge_detector_7;

architecture STRUCTURE of edge_detector_7 is
  signal \estado[2]_i_4_n_0\ : STD_LOGIC;
  signal \^sig_estado\ : STD_LOGIC;
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  sig_estado <= \^sig_estado\;
\estado[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E200FFFF"
    )
        port map (
      I0 => estado(0),
      I1 => \^sig_estado\,
      I2 => \estado_reg[0]_2\,
      I3 => reset_IBUF,
      I4 => EN_IBUF,
      O => \estado_reg[0]\
    );
\estado[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E200FFFF"
    )
        port map (
      I0 => estado(2),
      I1 => \^sig_estado\,
      I2 => \estado_reg[2]_0\,
      I3 => reset_IBUF,
      I4 => EN_IBUF,
      O => \estado_reg[2]\
    );
\estado[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFEFFAAFEFE"
    )
        port map (
      I0 => \estado[2]_i_4_n_0\,
      I1 => \estado_reg[0]_0\,
      I2 => \estado_reg[0]_1\,
      I3 => estado(1),
      I4 => estado(2),
      I5 => estado(0),
      O => \^sig_estado\
    );
\estado[2]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => sreg(0),
      I1 => estado(2),
      I2 => sreg(1),
      I3 => sreg(2),
      O => \estado[2]_i_4_n_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg[0]_0\(0),
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity segments is
  port (
    segmentMSG_OBUF : out STD_LOGIC_VECTOR ( 4 downto 0 );
    AN : out STD_LOGIC_VECTOR ( 2 downto 0 );
    segmentDECODED_OBUF : out STD_LOGIC_VECTOR ( 0 to 7 );
    estado : in STD_LOGIC_VECTOR ( 2 downto 0 );
    clk_IBUF_BUFG : in STD_LOGIC
  );
end segments;

architecture STRUCTURE of segments is
  signal \^an\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \AN[0]_i_1_n_0\ : STD_LOGIC;
  signal \AN[1]_i_1_n_0\ : STD_LOGIC;
  signal \AN[2]_i_1_n_0\ : STD_LOGIC;
  signal count_time : STD_LOGIC_VECTOR ( 0 to 18 );
  signal \count_time[0]_i_1_n_0\ : STD_LOGIC;
  signal \count_time[18]_i_2_n_0\ : STD_LOGIC;
  signal \count_time[18]_i_3_n_0\ : STD_LOGIC;
  signal \count_time[18]_i_4_n_0\ : STD_LOGIC;
  signal \count_time[18]_i_5_n_0\ : STD_LOGIC;
  signal count_time_0 : STD_LOGIC_VECTOR ( 18 to 18 );
  signal data0 : STD_LOGIC_VECTOR ( 18 downto 1 );
  signal \plusOp_carry__0_n_0\ : STD_LOGIC;
  signal \plusOp_carry__0_n_1\ : STD_LOGIC;
  signal \plusOp_carry__0_n_2\ : STD_LOGIC;
  signal \plusOp_carry__0_n_3\ : STD_LOGIC;
  signal \plusOp_carry__1_n_0\ : STD_LOGIC;
  signal \plusOp_carry__1_n_1\ : STD_LOGIC;
  signal \plusOp_carry__1_n_2\ : STD_LOGIC;
  signal \plusOp_carry__1_n_3\ : STD_LOGIC;
  signal \plusOp_carry__2_n_0\ : STD_LOGIC;
  signal \plusOp_carry__2_n_1\ : STD_LOGIC;
  signal \plusOp_carry__2_n_2\ : STD_LOGIC;
  signal \plusOp_carry__2_n_3\ : STD_LOGIC;
  signal \plusOp_carry__3_n_3\ : STD_LOGIC;
  signal plusOp_carry_n_0 : STD_LOGIC;
  signal plusOp_carry_n_1 : STD_LOGIC;
  signal plusOp_carry_n_2 : STD_LOGIC;
  signal plusOp_carry_n_3 : STD_LOGIC;
  signal \NLW_plusOp_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_plusOp_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \AN[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \AN[2]_i_1\ : label is "soft_lutpair0";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of plusOp_carry : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__2\ : label is 35;
  attribute ADDER_THRESHOLD of \plusOp_carry__3\ : label is 35;
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[0]_inst_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[1]_inst_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[2]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[3]_inst_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[4]_inst_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[5]_inst_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[6]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \segmentDECODED_OBUF[7]_inst_i_1\ : label is "soft_lutpair4";
begin
  AN(2 downto 0) <= \^an\(2 downto 0);
\AN[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \count_time[0]_i_1_n_0\,
      I1 => \^an\(0),
      O => \AN[0]_i_1_n_0\
    );
\AN[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^an\(0),
      I1 => \count_time[0]_i_1_n_0\,
      I2 => \^an\(1),
      O => \AN[1]_i_1_n_0\
    );
\AN[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^an\(1),
      I1 => \^an\(0),
      I2 => \count_time[0]_i_1_n_0\,
      I3 => \^an\(2),
      O => \AN[2]_i_1_n_0\
    );
\AN_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \AN[0]_i_1_n_0\,
      Q => \^an\(0),
      R => '0'
    );
\AN_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \AN[1]_i_1_n_0\,
      Q => \^an\(1),
      R => '0'
    );
\AN_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \AN[2]_i_1_n_0\,
      Q => \^an\(2),
      R => '0'
    );
\count_time[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \count_time[18]_i_5_n_0\,
      I1 => \count_time[18]_i_4_n_0\,
      I2 => \count_time[18]_i_3_n_0\,
      I3 => \count_time[18]_i_2_n_0\,
      I4 => count_time(18),
      O => \count_time[0]_i_1_n_0\
    );
\count_time[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FFFE"
    )
        port map (
      I0 => \count_time[18]_i_2_n_0\,
      I1 => \count_time[18]_i_3_n_0\,
      I2 => \count_time[18]_i_4_n_0\,
      I3 => \count_time[18]_i_5_n_0\,
      I4 => count_time(18),
      O => count_time_0(18)
    );
\count_time[18]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => count_time(14),
      I1 => count_time(15),
      I2 => count_time(13),
      I3 => count_time(12),
      O => \count_time[18]_i_2_n_0\
    );
\count_time[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFFF"
    )
        port map (
      I0 => count_time(1),
      I1 => count_time(0),
      I2 => count_time(3),
      I3 => count_time(2),
      I4 => count_time(16),
      I5 => count_time(17),
      O => \count_time[18]_i_3_n_0\
    );
\count_time[18]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_time(6),
      I1 => count_time(7),
      I2 => count_time(4),
      I3 => count_time(5),
      O => \count_time[18]_i_4_n_0\
    );
\count_time[18]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => count_time(11),
      I1 => count_time(10),
      I2 => count_time(8),
      I3 => count_time(9),
      O => \count_time[18]_i_5_n_0\
    );
\count_time_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(18),
      Q => count_time(0),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(8),
      Q => count_time(10),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(7),
      Q => count_time(11),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(6),
      Q => count_time(12),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(5),
      Q => count_time(13),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(4),
      Q => count_time(14),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(3),
      Q => count_time(15),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(2),
      Q => count_time(16),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(1),
      Q => count_time(17),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => count_time_0(18),
      Q => count_time(18),
      R => '0'
    );
\count_time_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(17),
      Q => count_time(1),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(16),
      Q => count_time(2),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(15),
      Q => count_time(3),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(14),
      Q => count_time(4),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(13),
      Q => count_time(5),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(12),
      Q => count_time(6),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(11),
      Q => count_time(7),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(10),
      Q => count_time(8),
      R => \count_time[0]_i_1_n_0\
    );
\count_time_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => data0(9),
      Q => count_time(9),
      R => \count_time[0]_i_1_n_0\
    );
plusOp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => plusOp_carry_n_0,
      CO(2) => plusOp_carry_n_1,
      CO(1) => plusOp_carry_n_2,
      CO(0) => plusOp_carry_n_3,
      CYINIT => count_time(18),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3) => count_time(14),
      S(2) => count_time(15),
      S(1) => count_time(16),
      S(0) => count_time(17)
    );
\plusOp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => plusOp_carry_n_0,
      CO(3) => \plusOp_carry__0_n_0\,
      CO(2) => \plusOp_carry__0_n_1\,
      CO(1) => \plusOp_carry__0_n_2\,
      CO(0) => \plusOp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3) => count_time(10),
      S(2) => count_time(11),
      S(1) => count_time(12),
      S(0) => count_time(13)
    );
\plusOp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \plusOp_carry__0_n_0\,
      CO(3) => \plusOp_carry__1_n_0\,
      CO(2) => \plusOp_carry__1_n_1\,
      CO(1) => \plusOp_carry__1_n_2\,
      CO(0) => \plusOp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3) => count_time(6),
      S(2) => count_time(7),
      S(1) => count_time(8),
      S(0) => count_time(9)
    );
\plusOp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \plusOp_carry__1_n_0\,
      CO(3) => \plusOp_carry__2_n_0\,
      CO(2) => \plusOp_carry__2_n_1\,
      CO(1) => \plusOp_carry__2_n_2\,
      CO(0) => \plusOp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3) => count_time(2),
      S(2) => count_time(3),
      S(1) => count_time(4),
      S(0) => count_time(5)
    );
\plusOp_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \plusOp_carry__2_n_0\,
      CO(3 downto 1) => \NLW_plusOp_carry__3_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \plusOp_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_plusOp_carry__3_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => data0(18 downto 17),
      S(3 downto 2) => B"00",
      S(1) => count_time(0),
      S(0) => count_time(1)
    );
\segmentDECODED_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \^an\(1),
      I1 => \^an\(0),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(0)
    );
\segmentDECODED_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \^an\(1),
      I1 => \^an\(0),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(1)
    );
\segmentDECODED_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \^an\(0),
      I1 => \^an\(1),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(2)
    );
\segmentDECODED_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => \^an\(1),
      I1 => \^an\(0),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(3)
    );
\segmentDECODED_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => \^an\(1),
      I1 => \^an\(0),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(4)
    );
\segmentDECODED_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^an\(1),
      I1 => \^an\(0),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(5)
    );
\segmentDECODED_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^an\(0),
      I1 => \^an\(1),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(6)
    );
\segmentDECODED_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^an\(1),
      I1 => \^an\(0),
      I2 => \^an\(2),
      O => segmentDECODED_OBUF(7)
    );
\segmentMSG_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6D666C66FFFFFFFF"
    )
        port map (
      I0 => \^an\(0),
      I1 => \^an\(1),
      I2 => estado(1),
      I3 => estado(2),
      I4 => estado(0),
      I5 => \^an\(2),
      O => segmentMSG_OBUF(4)
    );
\segmentMSG_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4D444644FFFFFFFF"
    )
        port map (
      I0 => \^an\(0),
      I1 => \^an\(1),
      I2 => estado(1),
      I3 => estado(2),
      I4 => estado(0),
      I5 => \^an\(2),
      O => segmentMSG_OBUF(3)
    );
\segmentMSG_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00DA000033FF3333"
    )
        port map (
      I0 => \^an\(0),
      I1 => \^an\(1),
      I2 => estado(0),
      I3 => estado(1),
      I4 => estado(2),
      I5 => \^an\(2),
      O => segmentMSG_OBUF(2)
    );
\segmentMSG_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"858557858585D585"
    )
        port map (
      I0 => \^an\(2),
      I1 => \^an\(0),
      I2 => \^an\(1),
      I3 => estado(2),
      I4 => estado(1),
      I5 => estado(0),
      O => segmentMSG_OBUF(1)
    );
\segmentMSG_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57575F5757575757"
    )
        port map (
      I0 => \^an\(2),
      I1 => \^an\(0),
      I2 => \^an\(1),
      I3 => estado(2),
      I4 => estado(1),
      I5 => estado(0),
      O => segmentMSG_OBUF(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sync is
  port (
    SYNC_OUT : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    botones_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end sync;

architecture STRUCTURE of sync is
  signal p_0_in : STD_LOGIC;
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
begin
SYNC_OUT_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => p_0_in,
      Q => SYNC_OUT,
      R => '0'
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => botones_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => p_0_in,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sync_0 is
  port (
    SYNC_OUT_reg_0 : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    botones_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of sync_0 : entity is "sync";
end sync_0;

architecture STRUCTURE of sync_0 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[1]\ : STD_LOGIC;
begin
SYNC_OUT_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[1]\,
      Q => SYNC_OUT_reg_0,
      R => '0'
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => botones_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg_n_0_[1]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sync_1 is
  port (
    SYNC_OUT_reg_0 : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    botones_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of sync_1 : entity is "sync";
end sync_1;

architecture STRUCTURE of sync_1 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[1]\ : STD_LOGIC;
begin
SYNC_OUT_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[1]\,
      Q => SYNC_OUT_reg_0,
      R => '0'
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => botones_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg_n_0_[1]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sync_2 is
  port (
    SYNC_OUT_reg_0 : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    botones_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of sync_2 : entity is "sync";
end sync_2;

architecture STRUCTURE of sync_2 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[1]\ : STD_LOGIC;
begin
SYNC_OUT_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[1]\,
      Q => SYNC_OUT_reg_0,
      R => '0'
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => botones_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg_n_0_[1]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sync_3 is
  port (
    SYNC_OUT_reg_0 : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of sync_3 : entity is "sync";
end sync_3;

architecture STRUCTURE of sync_3 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[1]\ : STD_LOGIC;
begin
SYNC_OUT_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[1]\,
      Q => SYNC_OUT_reg_0,
      R => '0'
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg_n_0_[1]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity deb_top is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg_reg : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg_reg_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg_reg_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    SYNC_OUT : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    \count_time_reg[0]\ : in STD_LOGIC;
    \count_time_reg[0]_0\ : in STD_LOGIC;
    \count_time_reg[0]_1\ : in STD_LOGIC;
    \count_time_reg[0]_2\ : in STD_LOGIC
  );
end deb_top;

architecture STRUCTURE of deb_top is
begin
deb_boton1: entity work.debouncer
     port map (
      D(0) => D(0),
      SYNC_OUT => SYNC_OUT,
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
deb_boton2: entity work.debouncer_8
     port map (
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \count_time_reg[0]_0\ => \count_time_reg[0]\,
      reg_reg_0(0) => reg_reg(0)
    );
deb_boton3: entity work.debouncer_9
     port map (
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \count_time_reg[0]_0\ => \count_time_reg[0]_0\,
      reg_reg_0(0) => reg_reg_0(0)
    );
deb_boton4: entity work.debouncer_10
     port map (
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \count_time_reg[0]_0\ => \count_time_reg[0]_1\,
      reg_reg_0(0) => reg_reg_1(0)
    );
deb_botonOK: entity work.debouncer_11
     port map (
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \count_time_reg[0]_0\ => \count_time_reg[0]_2\,
      reg_reg_0(0) => reg_reg_2(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ed_top is
  port (
    sig_estado : out STD_LOGIC;
    \sreg_reg[0]\ : out STD_LOGIC;
    \sreg_reg[2]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \estado_reg[0]\ : out STD_LOGIC;
    \estado_reg[2]\ : out STD_LOGIC;
    estado : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \estado_reg[0]_0\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    EN_IBUF : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_3\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end ed_top;

architecture STRUCTURE of ed_top is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal ed_boton1_n_0 : STD_LOGIC;
  signal ed_boton2_n_0 : STD_LOGIC;
  signal ed_boton2_n_1 : STD_LOGIC;
  signal ed_boton3_n_0 : STD_LOGIC;
  signal ed_boton4_n_0 : STD_LOGIC;
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[0]\ : STD_LOGIC;
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
  \sreg_reg[0]\ <= \^sreg_reg[0]\;
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
ed_boton1: entity work.edge_detector
     port map (
      D(0) => D(0),
      Q(2 downto 0) => sreg(2 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \sreg_reg[2]_0\ => ed_boton1_n_0
    );
ed_boton2: entity work.edge_detector_4
     port map (
      Q(2 downto 0) => sreg(2 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      estado(2 downto 0) => estado(2 downto 0),
      \estado_reg[0]\ => ed_boton2_n_0,
      \estado_reg[2]\ => ed_boton1_n_0,
      \estado_reg[2]_0\ => ed_boton3_n_0,
      \sreg_reg[0]_0\ => \^sreg_reg[0]\,
      \sreg_reg[0]_1\(0) => \sreg_reg[0]_0\(0),
      \sreg_reg[2]_0\ => ed_boton2_n_1
    );
ed_boton3: entity work.edge_detector_5
     port map (
      Q(2 downto 0) => \^q\(2 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \sreg_reg[0]_0\ => ed_boton3_n_0,
      \sreg_reg[0]_1\(0) => \sreg_reg[0]_1\(0),
      \sreg_reg[2]_0\ => \^sreg_reg[2]_0\
    );
ed_boton4: entity work.edge_detector_6
     port map (
      Q(2 downto 0) => \^q\(2 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      estado(0) => estado(2),
      \estado_reg[0]\ => ed_boton2_n_1,
      \estado_reg[0]_0\ => ed_boton1_n_0,
      \estado_reg[0]_1\ => \estado_reg[0]_0\,
      \estado_reg[0]_2\ => \^sreg_reg[2]_0\,
      \estado_reg[2]\ => ed_boton4_n_0,
      \sreg_reg[0]_0\(0) => \sreg_reg[0]_2\(0),
      \sreg_reg[2]_0\ => \sreg_reg[2]\
    );
edge_botonOK: entity work.edge_detector_7
     port map (
      EN_IBUF => EN_IBUF,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      estado(2 downto 0) => estado(2 downto 0),
      \estado_reg[0]\ => \estado_reg[0]\,
      \estado_reg[0]_0\ => \^sreg_reg[0]\,
      \estado_reg[0]_1\ => ed_boton3_n_0,
      \estado_reg[0]_2\ => ed_boton4_n_0,
      \estado_reg[2]\ => \estado_reg[2]\,
      \estado_reg[2]_0\ => ed_boton2_n_0,
      reset_IBUF => reset_IBUF,
      sig_estado => sig_estado,
      \sreg_reg[0]_0\(0) => \sreg_reg[0]_3\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sync_top is
  port (
    SYNC_OUT : out STD_LOGIC;
    SYNC_OUT_reg : out STD_LOGIC;
    SYNC_OUT_reg_0 : out STD_LOGIC;
    SYNC_OUT_reg_1 : out STD_LOGIC;
    SYNC_OUT_reg_2 : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    botones_IBUF : in STD_LOGIC_VECTOR ( 0 to 3 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end sync_top;

architecture STRUCTURE of sync_top is
begin
sync_boton1: entity work.sync
     port map (
      SYNC_OUT => SYNC_OUT,
      botones_IBUF(0) => botones_IBUF(0),
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
sync_boton2: entity work.sync_0
     port map (
      SYNC_OUT_reg_0 => SYNC_OUT_reg,
      botones_IBUF(0) => botones_IBUF(1),
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
sync_boton3: entity work.sync_1
     port map (
      SYNC_OUT_reg_0 => SYNC_OUT_reg_0,
      botones_IBUF(0) => botones_IBUF(2),
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
sync_boton4: entity work.sync_2
     port map (
      SYNC_OUT_reg_0 => SYNC_OUT_reg_1,
      botones_IBUF(0) => botones_IBUF(3),
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
sync_botonOK: entity work.sync_3
     port map (
      D(0) => D(0),
      SYNC_OUT_reg_0 => SYNC_OUT_reg_2,
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity signal_acond is
  port (
    sig_estado : out STD_LOGIC;
    \sreg_reg[0]\ : out STD_LOGIC;
    \sreg_reg[2]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \estado_reg[0]\ : out STD_LOGIC;
    \estado_reg[2]\ : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    estado : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \estado_reg[0]_0\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    EN_IBUF : in STD_LOGIC;
    botones_IBUF : in STD_LOGIC_VECTOR ( 0 to 3 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end signal_acond;

architecture STRUCTURE of signal_acond is
  signal SYNC_OUT : STD_LOGIC;
  signal antirrebotes_n_1 : STD_LOGIC;
  signal antirrebotes_n_2 : STD_LOGIC;
  signal antirrebotes_n_3 : STD_LOGIC;
  signal antirrebotes_n_4 : STD_LOGIC;
  signal reg : STD_LOGIC;
  signal sincronizadores_n_1 : STD_LOGIC;
  signal sincronizadores_n_2 : STD_LOGIC;
  signal sincronizadores_n_3 : STD_LOGIC;
  signal sincronizadores_n_4 : STD_LOGIC;
begin
antirrebotes: entity work.deb_top
     port map (
      D(0) => reg,
      SYNC_OUT => SYNC_OUT,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \count_time_reg[0]\ => sincronizadores_n_1,
      \count_time_reg[0]_0\ => sincronizadores_n_2,
      \count_time_reg[0]_1\ => sincronizadores_n_3,
      \count_time_reg[0]_2\ => sincronizadores_n_4,
      reg_reg(0) => antirrebotes_n_1,
      reg_reg_0(0) => antirrebotes_n_2,
      reg_reg_1(0) => antirrebotes_n_3,
      reg_reg_2(0) => antirrebotes_n_4
    );
detectores_de_flanco: entity work.ed_top
     port map (
      D(0) => reg,
      EN_IBUF => EN_IBUF,
      Q(2 downto 0) => Q(2 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      estado(2 downto 0) => estado(2 downto 0),
      \estado_reg[0]\ => \estado_reg[0]\,
      \estado_reg[0]_0\ => \estado_reg[0]_0\,
      \estado_reg[2]\ => \estado_reg[2]\,
      reset_IBUF => reset_IBUF,
      sig_estado => sig_estado,
      \sreg_reg[0]\ => \sreg_reg[0]\,
      \sreg_reg[0]_0\(0) => antirrebotes_n_1,
      \sreg_reg[0]_1\(0) => antirrebotes_n_2,
      \sreg_reg[0]_2\(0) => antirrebotes_n_3,
      \sreg_reg[0]_3\(0) => antirrebotes_n_4,
      \sreg_reg[2]\ => \sreg_reg[2]\,
      \sreg_reg[2]_0\ => \sreg_reg[2]_0\
    );
sincronizadores: entity work.sync_top
     port map (
      D(0) => D(0),
      SYNC_OUT => SYNC_OUT,
      SYNC_OUT_reg => sincronizadores_n_1,
      SYNC_OUT_reg_0 => sincronizadores_n_2,
      SYNC_OUT_reg_1 => sincronizadores_n_3,
      SYNC_OUT_reg_2 => sincronizadores_n_4,
      botones_IBUF(0 to 3) => botones_IBUF(0 to 3),
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    clk : in STD_LOGIC;
    bloqueo : in STD_LOGIC;
    reset : in STD_LOGIC;
    EN : in STD_LOGIC;
    botones : in STD_LOGIC_VECTOR ( 0 to 3 );
    salidaOK : out STD_LOGIC;
    segmentDECODED : out STD_LOGIC_VECTOR ( 0 to 7 );
    segmentMSG : out STD_LOGIC_VECTOR ( 0 to 6 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of top : entity is true;
end top;

architecture STRUCTURE of top is
  signal AN : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal EN_IBUF : STD_LOGIC;
  signal acondicionamiento_entradas_n_1 : STD_LOGIC;
  signal acondicionamiento_entradas_n_2 : STD_LOGIC;
  signal acondicionamiento_entradas_n_3 : STD_LOGIC;
  signal acondicionamiento_entradas_n_7 : STD_LOGIC;
  signal acondicionamiento_entradas_n_8 : STD_LOGIC;
  signal bloqueo_IBUF : STD_LOGIC;
  signal botones_IBUF : STD_LOGIC_VECTOR ( 0 to 3 );
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal \detectores_de_flanco/ed_boton4/sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal estado : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal maquina_de_estado_n_6 : STD_LOGIC;
  signal reset_IBUF : STD_LOGIC;
  signal salidaOK_OBUF : STD_LOGIC;
  signal segmentDECODED_OBUF : STD_LOGIC_VECTOR ( 0 to 7 );
  signal segmentMSG_OBUF : STD_LOGIC_VECTOR ( 0 to 6 );
  signal sig_estado : STD_LOGIC;
begin
EN_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => EN,
      O => EN_IBUF
    );
acondicionamiento_entradas: entity work.signal_acond
     port map (
      D(0) => bloqueo_IBUF,
      EN_IBUF => EN_IBUF,
      Q(2 downto 0) => \detectores_de_flanco/ed_boton4/sreg\(2 downto 0),
      botones_IBUF(0 to 3) => botones_IBUF(0 to 3),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      estado(2 downto 0) => estado(2 downto 0),
      \estado_reg[0]\ => acondicionamiento_entradas_n_7,
      \estado_reg[0]_0\ => maquina_de_estado_n_6,
      \estado_reg[2]\ => acondicionamiento_entradas_n_8,
      reset_IBUF => reset_IBUF,
      sig_estado => sig_estado,
      \sreg_reg[0]\ => acondicionamiento_entradas_n_1,
      \sreg_reg[2]\ => acondicionamiento_entradas_n_2,
      \sreg_reg[2]_0\ => acondicionamiento_entradas_n_3
    );
bloqueo_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => bloqueo,
      O => bloqueo_IBUF
    );
\botones_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => botones(0),
      O => botones_IBUF(0)
    );
\botones_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => botones(1),
      O => botones_IBUF(1)
    );
\botones_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => botones(2),
      O => botones_IBUF(2)
    );
\botones_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => botones(3),
      O => botones_IBUF(3)
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
maquina_de_estado: entity work.FSM_psw
     port map (
      AN(2 downto 0) => AN(2 downto 0),
      EN_IBUF => EN_IBUF,
      Q(2 downto 0) => \detectores_de_flanco/ed_boton4/sreg\(2 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      estado(2 downto 0) => estado(2 downto 0),
      \estado_reg[0]_0\ => maquina_de_estado_n_6,
      \estado_reg[0]_1\ => acondicionamiento_entradas_n_7,
      \estado_reg[1]_0\ => acondicionamiento_entradas_n_1,
      \estado_reg[1]_1\ => acondicionamiento_entradas_n_2,
      \estado_reg[1]_2\ => acondicionamiento_entradas_n_3,
      \estado_reg[2]_0\ => acondicionamiento_entradas_n_8,
      reset_IBUF => reset_IBUF,
      salidaOK_OBUF => salidaOK_OBUF,
      segmentMSG_OBUF(1) => segmentMSG_OBUF(0),
      segmentMSG_OBUF(0) => segmentMSG_OBUF(4),
      sig_estado => sig_estado
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
salidaOK_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => salidaOK_OBUF,
      O => salidaOK
    );
\segmentDECODED_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(0),
      O => segmentDECODED(0)
    );
\segmentDECODED_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(1),
      O => segmentDECODED(1)
    );
\segmentDECODED_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(2),
      O => segmentDECODED(2)
    );
\segmentDECODED_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(3),
      O => segmentDECODED(3)
    );
\segmentDECODED_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(4),
      O => segmentDECODED(4)
    );
\segmentDECODED_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(5),
      O => segmentDECODED(5)
    );
\segmentDECODED_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(6),
      O => segmentDECODED(6)
    );
\segmentDECODED_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentDECODED_OBUF(7),
      O => segmentDECODED(7)
    );
\segmentMSG_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentMSG_OBUF(0),
      O => segmentMSG(0)
    );
\segmentMSG_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentMSG_OBUF(1),
      O => segmentMSG(1)
    );
\segmentMSG_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentMSG_OBUF(2),
      O => segmentMSG(2)
    );
\segmentMSG_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentMSG_OBUF(3),
      O => segmentMSG(3)
    );
\segmentMSG_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentMSG_OBUF(4),
      O => segmentMSG(4)
    );
\segmentMSG_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentMSG_OBUF(5),
      O => segmentMSG(5)
    );
\segmentMSG_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => segmentMSG_OBUF(6),
      O => segmentMSG(6)
    );
segmentos: entity work.segments
     port map (
      AN(2 downto 0) => AN(2 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      estado(2 downto 0) => estado(2 downto 0),
      segmentDECODED_OBUF(0 to 7) => segmentDECODED_OBUF(0 to 7),
      segmentMSG_OBUF(4) => segmentMSG_OBUF(1),
      segmentMSG_OBUF(3) => segmentMSG_OBUF(2),
      segmentMSG_OBUF(2) => segmentMSG_OBUF(3),
      segmentMSG_OBUF(1) => segmentMSG_OBUF(5),
      segmentMSG_OBUF(0) => segmentMSG_OBUF(6)
    );
end STRUCTURE;
